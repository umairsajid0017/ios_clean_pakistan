//
//  ImageTextField.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//


import Foundation
import UIKit

@IBDesignable class ImageTextField: UITextField {
    
    
    @IBInspectable var image: UIImage = UIImage()
    
    @IBInspectable var borderColor: UIColor = UIColor.black
    @IBInspectable var paddingLeft: Int = 0
    
    
    @IBInspectable var corners: Int = 0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

         let marginView = UIView(frame: CGRect(x: 0, y: 0, width: paddingLeft+10, height: 40))
        marginView.backgroundColor = self.backgroundColor

        let imageView = UIImageView(frame: CGRect(x: 15, y: 8 , width: 24, height: 24))

        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        marginView.addSubview(imageView)

        self.leftView = marginView
        self.leftViewMode = UITextField.ViewMode.always
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = borderColor.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        
        
        self.layer.masksToBounds = true
        
        self.layer.cornerRadius = CGFloat(corners)
        self.clipsToBounds=true

    }

    override func prepareForInterfaceBuilder() {
        
    }
    
    
}
