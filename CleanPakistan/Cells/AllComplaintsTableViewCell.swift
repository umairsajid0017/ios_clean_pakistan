//
//  AllComplaintsTableViewCell.swift
//  CleanPakistan
//
//  Created by Apple on 30/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit

class AllComplaintsTableViewCell: UITableViewCell {
    @IBOutlet weak var complaintImage: UIImageView!
    
    //Labels
    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var statusIdLbl: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var remarksLbl: UILabel!
    @IBOutlet weak var garbagelbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var areaLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var directionButton: UIButton!
    @IBOutlet weak var locationLbl: UILabel!
    
    
    
    //Delete View
    @IBOutlet weak var D_DirectionButton: UIButton!
    @IBOutlet weak var D_ViewButton: UIButton!
    @IBOutlet weak var DeleteButton: UIButton!
    //Feedback View
    @IBOutlet weak var F_DirectionButton: UIButton!
    @IBOutlet weak var F_ViewButton: UIButton!
    @IBOutlet weak var FeedbackButton: UIButton!
    //Respond View
    @IBOutlet weak var R_DirectionButton: UIButton!
    @IBOutlet weak var R_ViewButton: UIButton!
   // @IBOutlet weak var RespondButton: UIButton!
    @IBOutlet weak var respondButton: UIButton!
    //Close View
    @IBOutlet weak var C_DirectionButton: UIButton!
    @IBOutlet weak var C_ViewButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    //Direction View
    @IBOutlet weak var UcDirectionButton: UIButton!
    @IBOutlet weak var viewButton: UIButton!
    
  
    
    //Buttons View
    @IBOutlet weak var DirectionView: UIView!
    @IBOutlet weak var DeleteView: UIView!
    @IBOutlet weak var FeedbackView: UIView!
    @IBOutlet weak var RespondView: UIView!
    @IBOutlet weak var CloseView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
