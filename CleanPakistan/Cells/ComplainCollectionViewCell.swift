//
//  ComplainCollectionViewCell.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 12/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit

class ComplainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backView:UIView!
    
    @IBOutlet weak var Picture: UIImageView!
    @IBOutlet weak var PictureLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    
}
