//
//  NotificationsTableViewCell.swift
//  CleanPakistan
//
//  Created by Apple on 17/12/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {
    @IBOutlet weak var notificationIdLbl: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var remarksLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var complaintImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
