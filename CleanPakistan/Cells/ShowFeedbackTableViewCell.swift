//
//  ShowFeedbackTableViewCell.swift
//  CleanPakistan
//
//  Created by Apple on 30/12/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Cosmos

class ShowFeedbackTableViewCell: UITableViewCell {

    @IBOutlet weak var complaintIdLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var comentLabel: UILabel!
    @IBOutlet weak var respondLabel: UILabel!
    @IBOutlet weak var satisfyLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
