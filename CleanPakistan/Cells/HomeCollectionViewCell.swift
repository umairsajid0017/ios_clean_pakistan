//
//  HomeCollectionViewCell.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 12/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
        @IBOutlet weak var ComplaintCount: UILabel!
        @IBOutlet weak var ComplaintDescription: UILabel!
        @IBOutlet weak var ComplaintPicture: UIImageView!
        
    
    
}
