//
//  BaseViewController.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 12/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//


import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var LogoutBtn: UIBarButtonItem!
    
    func addSideMenu(){
        menuButton.target = self.revealViewController()
        
     //   LogoutBtn.target = self.revealViewController()
        menuButton.action = #selector(RevealViewController.revealLeftView)
     //   LogoutBtn.action = #selector(RevealViewController.revealRightView)
        self.revealViewController()?.leftViewRevealWidth=UIScreen.main.bounds.width - 80
    }

    func addNavBars(_ title:String){

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 22),NSAttributedString.Key.foregroundColor:UIColor.white]
        
        self.navigationItem.title = title
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func LogoutButton(_ sender: UIBarButtonItem) {
        print("Logout")
    }
    

}




