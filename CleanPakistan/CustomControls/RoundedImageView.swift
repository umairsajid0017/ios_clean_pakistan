//
//  RoundedImageView.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class RoundedImageView: UIImageView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func commonInit(){
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.commonInit()
        
    }
    
}
