//
//  RoundedShadowView.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation

import UIKit

class RoundedShadowView: UIView {
    
    
    @IBInspectable var borderColor: UIColor = UIColor.red
    @IBInspectable var borderWidth: CGFloat = 1.0
    
    
    let containerView = UIView()
    let cornerRadius: CGFloat = 4.0

    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutView()
    }
    
    func layoutView() {
        
        // set the shadow of the view's layer
        layer.backgroundColor = UIColor.clear.cgColor
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowOpacity = 0.0
        layer.shadowRadius = 4.0
        
        // set the cornerRadius of the containerView's layer
        containerView.layer.borderColor = borderColor.cgColor
        containerView.layer.borderWidth = borderWidth
        containerView.layer.cornerRadius = cornerRadius
        containerView.layer.masksToBounds = true
        
        containerView.backgroundColor = UIColor.white
        addSubview(containerView)
        sendSubviewToBack(containerView)

        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        // pin the containerView to the edges to the view
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
