//
//  ShadowButton.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class ShadowButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.red
    @IBInspectable var borderWidth: CGFloat = 0.0
    
    @IBInspectable var cornerRadius: CGFloat = 4.0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.commonInit()
    }
    
    func commonInit(){
      //  self.layer.cornerRadius = 2.0
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width:0.0, height:1.5)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.4
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.size.height / 2

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

    }
    
    override func prepareForInterfaceBuilder() {
        
    }
    


}
