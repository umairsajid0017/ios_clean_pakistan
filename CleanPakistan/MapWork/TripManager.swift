//
//  TripManager.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//


import Foundation
import UIKit
import CoreLocation


var controller : UIViewController! = nil
var sharedTripInstance : TripManager! = nil
var currentBookingId = ""
class TripManager : NSObject
{
    var tempPickupLocation : SelectLocationModel! = nil
    var tempDestinationLocation : SelectLocationModel! = nil
    var nearbyLocations : [SelectLocationModel]! = nil
    
    override init()
    {
        super.init();
    }
    
    class func sharedManager() -> TripManager
    {
        if let _ = sharedTripInstance
        {
            return sharedTripInstance;
        }
        sharedTripInstance = TripManager();
        return sharedTripInstance;
    }
    
}
