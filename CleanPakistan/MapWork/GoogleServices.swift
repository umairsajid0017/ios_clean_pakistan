//
//  Services.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GooglePlaces


protocol GoogleServicesDelegate
{
    func didFailedGoogleServiceWithError(errorMessage : String, serviceType : ServicesType);
    func didRecievedGoogleServiceResponse(response : Any, serviceType : ServicesType);
}

class GoogleServices : NSObject{
    var googleServicesDelegate : GoogleServicesDelegate! = nil;
    
    
    convenience init(delegate : GoogleServicesDelegate?)
    {
        self.init();
        self.googleServicesDelegate = delegate;
    }
    
    override init()
    {
        super.init();
    }
    
    
    func executeGooglePlacesAPIForNearbyLocation(coordinates : CLLocationCoordinate2D)
    {
        let placesClient = GMSPlacesClient.shared()
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            if let placeLikelihoodList = placeLikelihoodList {
                var googlePlaces = [SelectLocationModel]()
                for likelihood in placeLikelihoodList.likelihoods {
                    let place = likelihood.place
                    let model : SelectLocationModel = SelectLocationModel()
                    model.coordinates = place.coordinate
                    model.name = place.name
                    model.placeId = place.placeID
                    if let address = place.formattedAddress {
                        model.address = address
                    }
                    googlePlaces.append(model)
                }
                if let del = self.googleServicesDelegate{
                    del.didRecievedGoogleServiceResponse(response: googlePlaces, serviceType: .NearbyGooglePlaces);
                }
            }
        })
    }
    
    func executeSearchGooglePlacesAPIForNearbyLocation(coordinates : CLLocationCoordinate2D, keyword : String)
    {
        let filter = GMSAutocompleteFilter()
       // filter.country = "sa"
        let placesClient = GMSPlacesClient.shared()
        placesClient.autocompleteQuery(keyword, bounds: nil, filter: filter, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            var googlePlaces = [SelectLocationModel]()
            if let results = results {
                for result in results {
                    let model : SelectLocationModel = SelectLocationModel()
                    model.name = result.attributedPrimaryText.string
                    if let address = result.attributedSecondaryText {
                        model.address = address.string
                    }
                    model.placeId = result.placeID
                    
                    googlePlaces.append(model)
                }
                if let del = self.googleServicesDelegate
                {
                    del.didRecievedGoogleServiceResponse(response: googlePlaces, serviceType: .NearbyGooglePlaces)
                }
            }
        })
    }
    
    func executeGetPlaceDataByPlaceId(placeID: String){
        let placesClient = GMSPlacesClient.shared()
        placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeID)")
                return
            }
            
            let model : SelectLocationModel = SelectLocationModel()
            model.coordinates = place.coordinate
            model.name = place.name
            model.placeId = place.placeID
            if let address = place.formattedAddress {
                model.address = address
            }
            
            if let del = self.googleServicesDelegate
            {
                del.didRecievedGoogleServiceResponse(response: model, serviceType: .PlaceDataByPlaceId);
            }
        })
    }

}
