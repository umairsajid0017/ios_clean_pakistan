//
//  Constant.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation

//Google Map keys
let GOOGLE_MAP_API_KEY = "AIzaSyBW8OcJXYMjCActFVp1oeZwKCN5dU5a4b8"
let GOOGLE_PLACES_SEARCH_API_KEY = "AIzaSyBW8OcJXYMjCActFVp1oeZwKCN5dU5a4b8"

//CELL
let PICKUP_DROPOFF_TABLE_VIEW_CELL = "PickupDropoffTableViewCell"

//Google APIS
let APIURL_GOOGLE_PLACES = "https://maps.googleapis.com/maps/api/place/search/json?radius=500&sensor=true&key=\(GOOGLE_PLACES_SEARCH_API_KEY)"
let APIURL_GOOGLE_DIRECTIONS = "https://maps.googleapis.com/maps/api/directions/json?key=\(GOOGLE_PLACES_SEARCH_API_KEY)&mode=driving"
let APIURL_SEARCH_GOOGLE_PLACES = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(GOOGLE_PLACES_SEARCH_API_KEY)";

