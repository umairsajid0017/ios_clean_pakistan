//
//  LocationSelectViewController.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 10/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//


import Foundation
import UIKit
import CoreLocation
import GoogleMaps


protocol SelectLocationControllerDelegate : NSObjectProtocol {
    func SelectLocationControllerDidSelect(model : SelectLocationModel, type : SelectLocationMode)
}

class SelectLocationViewController : UIViewController
{
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var staticMapPin: UIImageView!
    @IBOutlet weak var staticMapContainer: UIView!
    @IBOutlet weak var recentsNearbyTableView: UITableView!
    @IBOutlet weak var recentsNearbyTableViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var recentsNearbyTableViewBlockerButton: UIButton!

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchContainer: UIView!

    var mapView : GMSMapView! = nil
    var camera : GMSCameraPosition! = nil
    var currentLocationOfUser : CLLocation! = nil
    var selectLocationMode : SelectLocationMode = .LocationDestination
    var recentsNearbyDataSource : PickupDropoffDataSource! = nil

    weak var delegate : SelectLocationControllerDelegate?
    
    var locationManager : CLLocationManager! = CLLocationManager()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
//
//        self.recentsNearbyDataSource = PickupDropoffDataSource();
//        self.recentsNearbyDataSource.setup(controller: self, tableView: self.recentsNearbyTableView);
      //  let any : Any? = self.setupGoogleMapForLocation(location: self.currentLocationOfUser.coordinate)
    //    if any == nil {
    //        print("nil") // nil
    //    }
        

        self.setupGoogleMapForLocation(location: self.currentLocationOfUser.coordinate)
        
        
        self.updateViewsForLocationSelection()
//
        self.recentsNearbyTableView.isHidden = true
        self.recentsNearbyTableViewBlockerButton.isHidden = true
//
       // self.executeNearbyLocationsService()
    }

    
    func updateViewsForLocationSelection()
    {
        self.searchContainer.isHidden = false
        self.recentsNearbyTableView.isHidden = false
        self.btnDone.isHidden = false
    }
    
    func executeNearbyLocationsService()
    {
        if TripManager.sharedManager().nearbyLocations == nil {
            let nearbyLocationsService = GoogleServices(delegate: self)
            nearbyLocationsService.executeGooglePlacesAPIForNearbyLocation(coordinates: self.currentLocationOfUser.coordinate)
        }
        else {
            recentsNearbyDataSource.nearbyArray = TripManager.sharedManager().nearbyLocations
            self.recentsNearbyTableView.reloadData()
        }
    }
    
    func executeNearbySearchLocationsService(searchText : String){
        let nearbyLocationsService = GoogleServices(delegate: self);
        nearbyLocationsService.executeSearchGooglePlacesAPIForNearbyLocation(coordinates: self.currentLocationOfUser.coordinate, keyword: searchText);
    }
    
    
    @IBAction func doneAction(_ sender: Any){
        self.removeSecondDoneButton()
    }
    
    func removeSecondDoneButton(){
        self.backAction(sender: UIButton())
    }

    @IBAction func backAction(sender: AnyObject) {
        if self.selectLocationMode == .RideLater || self.selectLocationMode == .RideNow {
            TripManager.sharedManager().tempPickupLocation = nil
        }
        DaliRideUtility.sharedUtility().performBackAction(controller: self, animated: true);
    }
    
    @IBAction func tableViewBlockerTapped(sender: AnyObject){
        self.recentsNearbyTableViewConstraint.priority = UILayoutPriority(rawValue: 999)
        self.recentsNearbyTableViewBlockerButton.isHidden = true
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (completed) in
            
        })
    }
}

extension SelectLocationViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true);
        //pin navigation mode selected
        if indexPath.section == 0
        {
            if indexPath.row == 0 {
                self.recentsNearbyTableViewBlockerButton.isHidden = false
                self.recentsNearbyTableViewConstraint.priority = UILayoutPriority(rawValue: 1)
                self.staticMapPin.isHidden = false
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (completed) in
                
                })
            } else {
                let model = self.recentsNearbyDataSource.recentsArray[indexPath.row] as! SelectLocationModel
                if self.delegate != nil {
                    self.delegate!.SelectLocationControllerDidSelect(model: model, type: self.selectLocationMode)
                    self.backAction(sender: UIButton())
                }
            }
        } else {
            let model = self.recentsNearbyDataSource.nearbyArray[indexPath.row]
            if self.delegate != nil {
                if model.coordinates != nil {
                    self.delegate!.SelectLocationControllerDidSelect(model: model, type: self.selectLocationMode)
                    self.backAction(sender: UIButton())
                } else {
                    let nearbyLocationsService = GoogleServices(delegate: self);
                    nearbyLocationsService.executeGetPlaceDataByPlaceId(placeID: model.placeId)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }
}


extension SelectLocationViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        {
            if text.count > 0
            {
                self.executeNearbySearchLocationsService(searchText: text)
            }
        }
        return true
    }
}

extension SelectLocationViewController : GoogleServicesDelegate
{
    func didFailedGoogleServiceWithError(errorMessage: String, serviceType: ServicesType)
    {
    }
    
    func didRecievedGoogleServiceResponse(response: Any, serviceType: ServicesType)
    {
        if serviceType == .NearbyGooglePlaces {
            
            guard let response = response as? [SelectLocationModel] else {
                return
            }
            
            
            recentsNearbyDataSource.nearbyArray = response //as? [SelectLocationModel]
            self.recentsNearbyTableView.reloadData()
        } else if serviceType == .PlaceDataByPlaceId {
            if self.delegate != nil {
                self.delegate!.SelectLocationControllerDidSelect(model: response as! SelectLocationModel, type: self.selectLocationMode)
                self.backAction(sender: UIButton())
            }
        }
    }
}

extension SelectLocationViewController : GMSMapViewDelegate
{
    func setupGoogleMapForLocation(location : CLLocationCoordinate2D?)
    {
        var initialLocation = CLLocationCoordinate2D(latitude: 23.4241, longitude: 53.8478)
        if camera == nil
        {
            if let _ = location
            {
                initialLocation = location!
            }
            
            camera = GMSCameraPosition.camera(withTarget: initialLocation, zoom: 17.0);
        }
        
        if mapView == nil
        {
            mapView = GMSMapView.map(withFrame: self.staticMapContainer.bounds, camera: camera)
            mapView.translatesAutoresizingMaskIntoConstraints = false
            Constraints.sharedConstraints().addEdgeConstraints(parentView: self.staticMapContainer, subView: mapView, left: 0, top: 0, right: 0, bottom: 0)

            mapView.isMyLocationEnabled = true;
            mapView.delegate = self
            DispatchQueue.main.async {
                self.mapView.isMyLocationEnabled = true;
            };
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        if !staticMapPin.isHidden
        {
            let rect = CGRect(origin: self.staticMapPin.frame.origin, size: self.staticMapPin.frame.size);
            let point = CGPoint(x:rect.midX, y:rect.midY);
            
            let coordinates = self.mapView.projection.coordinate(for: point);
            self.reverseGeoCode(coordinates: coordinates);
        }
    }
    
    func reverseGeoCode(coordinates : CLLocationCoordinate2D)
    {
        let handler : GMSReverseGeocodeCallback = {(response : GMSReverseGeocodeResponse?, error : Error?) -> Void in
            if let address = response?.firstResult()
            {
                DispatchQueue.main.async {
                    if let lines = address.lines?.first
                    {
                        
                        var nameLocation = ""
                        var addressLocation = ""
                        if let name = address.thoroughfare {
                            nameLocation = name
                        }
                        if let subLocality = address.subLocality{
                            addressLocation = "\(subLocality)"
                        }
                        if let locality = address.locality{
                            addressLocation = addressLocation + " \(locality)"
                        }
                        if let country = address.country {
                            addressLocation = addressLocation + " \(country)"
                        }
                        let model = SelectLocationModel()
                        
                        model.name = nameLocation
                        model.address = addressLocation
                        model.coordinates = coordinates
                        if self.delegate != nil {
                            self.delegate!.SelectLocationControllerDidSelect(model: model, type: self.selectLocationMode)
                        }
                        self.searchField.text = nameLocation
                    }
                }
            }
            else
            {
                print("failed to ververse geocode");
            }
        }
        let geocoder = GMSGeocoder();
        geocoder.reverseGeocodeCoordinate(coordinates, completionHandler: handler);
    }
}

extension SelectLocationViewController : SelectLocationControllerDelegate {
    func updateLocationViews(){    }
    
    func SelectLocationControllerDidSelect(model: SelectLocationModel, type: SelectLocationMode) {
        if type == .LocationPickup {
            TripManager.sharedManager().tempPickupLocation = model
        } else if type == .LocationDestination {
            TripManager.sharedManager().tempDestinationLocation = model
        }
    
        self.updateLocationViews()

    }
}

extension SelectLocationViewController : CLLocationManagerDelegate
{
    func setupLocationManager()
    {
        if locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)){
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location : CLLocation = locations.last {
            locationManager.stopUpdatingLocation()
            self.currentLocationOfUser = location
            self.setupGoogleMapForLocation(location: location.coordinate)
            //self.executeNearbyLocationsService()
        }
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        print("location manager updates paused");
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        print("location manager updates resumed");
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error is \(error.localizedDescription)");
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Authorization status is \(status)");
    }
}
