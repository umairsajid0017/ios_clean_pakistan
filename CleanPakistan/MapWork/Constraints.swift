//
//  Constraints.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//


import Foundation
import UIKit

var sharedConstraintsInstance : Constraints! = nil;

class Constraints : NSObject
{
    class func sharedConstraints() -> Constraints
    {
        if sharedConstraintsInstance == nil
        {
            sharedConstraintsInstance = Constraints();
        }
        return sharedConstraintsInstance;
    }
    
    
    func addEdgeConstraints(parentView : UIView, subView : UIView, left: NSInteger, top: NSInteger, right: NSInteger, bottom: NSInteger)
    {
        subView.translatesAutoresizingMaskIntoConstraints = false;
        parentView.addSubview(subView);
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(top)-[subView]-\(bottom)-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["subView":subView]);
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(left)-[subView]-\(right)-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["subView":subView]);
        parentView.addConstraints(verticalConstraints);
        parentView.addConstraints(horizontalConstraints);
    }
    
    func addBottomConstraint(parentView : UIView, subView : UIView, left: NSInteger, right: NSInteger, bottom: NSInteger, height : NSInteger)
    {
        subView.translatesAutoresizingMaskIntoConstraints = false;
        parentView.addSubview(subView);
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:[subView(\(height))]-\(bottom)-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["subView":subView]);
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(left)-[subView]-\(right)-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["subView":subView]);
        parentView.addConstraints(verticalConstraints);
        parentView.addConstraints(horizontalConstraints);
    }
}
