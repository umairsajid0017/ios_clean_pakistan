//
//  SelectLocationModel.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class SelectLocationModel : NSObject, NSCoding {
    var placeId : String! = nil
    var coordinates : CLLocationCoordinate2D! = nil
    var name : String! = nil
    var address : String! = nil
    var locationType : LocationType = .None
    var isAirport = false
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init()
        
        self.placeId = aDecoder.decodeObject(forKey: "placeId") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.locationType = LocationType(rawValue: Int(aDecoder.decodeCInt(forKey: "locationType"))) ?? .None
        self.isAirport = aDecoder.decodeBool(forKey: "isAirport")
        
        let latitude = aDecoder.decodeDouble(forKey: "latitude")
        let longitude = aDecoder.decodeDouble(forKey: "longitude")
        self.coordinates = CLLocationCoordinate2DMake(latitude, longitude)
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.placeId, forKey: "placeId")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.address, forKey: "address")
        aCoder.encodeCInt(Int32(self.locationType.rawValue), forKey: "locationType")
        aCoder.encode(self.coordinates.latitude, forKey: "latitude")
        aCoder.encode(self.coordinates.longitude, forKey: "longitude")
        aCoder.encode(self.isAirport, forKey: "isAirport")
    }
}
