//
//  Utility.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation

import UIKit

enum ServicesType : NSInteger
{
    case None
    case NearbyGooglePlaces
    case PlaceDataByPlaceId

}



enum MapViewType : Int
{
    case Normal = 1
    case InRideLater = 5
}

enum LocationType : Int
{
    case None = 0
    case Pickup = 1
    case Destination = 2
    case Home = 3
    case Work = 4
}

enum SelectLocationMode : Int
{
    case None = 0;
    case LocationPickup = 1
    case LocationDestination = 2
    case RideNow = 3
    case RideLater = 4
    case ShortCutHome = 5
    case ShortCutWork = 6
}

import Foundation
import UIKit
import CoreLocation
import GoogleMaps

var sharedInstance : DaliRideUtility! = nil;
class DaliRideUtility : NSObject{
    class func sharedUtility() -> DaliRideUtility
    {
        if sharedInstance == nil
        {
            sharedInstance = DaliRideUtility();
        }
        return sharedInstance;
    }
    func performBackAction(controller : UIViewController, animated : Bool)
    {
        if let _ = controller.navigationController
        {
            let _ = controller.navigationController?.popViewController(animated: animated);
        }
        else
        {
            controller.dismiss(animated: animated, completion: nil);
        }
    }
    
}

