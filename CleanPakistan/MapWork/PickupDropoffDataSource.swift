//
//  PickupDropoffDataSource.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 11/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//


import Foundation
import UIKit

class PickupDropoffDataSource : NSObject, UITableViewDelegate, UITableViewDataSource
{
    var recentsArray : [Any]! = nil;
    var nearbyArray : [SelectLocationModel]! = nil;
    weak var tableController : UIViewController! = nil;
    var selectedIndexPath : IndexPath? = IndexPath(row: 0, section: 0);
    override init()
    {
        super.init();
        recentsArray = [String]();
        nearbyArray = [SelectLocationModel]();
        self.populateData();
    }
    
    func populateData()
    {
        recentsArray.append("Set Pin Location")
    }
    
    func setup(controller : UIViewController, tableView : UITableView)
    {
        tableView.dataSource = self;
        let nib = UINib(nibName: PICKUP_DROPOFF_TABLE_VIEW_CELL, bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: PICKUP_DROPOFF_TABLE_VIEW_CELL);
        self.tableController = controller;
        tableView.reloadData();
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return recentsArray.count;
        }
        else
        {
            return nearbyArray.count;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50;
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell");
                if cell == nil {
                    cell = UITableViewCell(style: .default, reuseIdentifier: "cell");
                }
                if #available(iOS 8.2, *) {
                    cell?.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
                }
                cell?.textLabel?.text = recentsArray[indexPath.row] as? String
                return cell!
            } else {
                
                let model = recentsArray[indexPath.row] as! SelectLocationModel
                
                let cell = tableView.dequeueReusableCell(withIdentifier: PICKUP_DROPOFF_TABLE_VIEW_CELL) as! PickupDropoffTableViewCell
                if model.locationType == .Home {
                    cell.lblLocation.text = "Home"
                } else {
                    cell.lblLocation.text = "Work"
                }
                cell.lblDistance.text = model.address
                return cell;
            }
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: PICKUP_DROPOFF_TABLE_VIEW_CELL) as! PickupDropoffTableViewCell;
             cell.lblLocation.text = nearbyArray[indexPath.row].name;
             cell.lblDistance.text = nearbyArray[indexPath.row].address;
            return cell;
        }
    }
}
