//
//  Classes.swift
//  CleanPakistan
//
//  Created by Apple on 25/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation
import Alamofire

import UIKit
//ViewDetailsComplaints.php showAllNotifications.php
class AppConfig {
    static var BaseUrl = "http://cleanpakistan.nwsols.com/cleanpakistan/"
    public static var loginUrl = BaseUrl+"login.php"
    public static var forgetUrl = BaseUrl+".php"
    public static var signupUrl = BaseUrl+"register.php"
    public static var getAllUCAreas = BaseUrl+"showAllUCAreas.php"
    public static var getAllCitiesUrl = BaseUrl+"showAllCity.php"
    public static var insertComplaints = BaseUrl+"insertComplaints.php"
    public static var getAllComplaintsUrl = BaseUrl+"showAllComplaints.php"
    public static var getAllPendingUrl = BaseUrl+"showAllPendingComplaint.php"
    public static var getAllClosedUrl = BaseUrl+"showAllFinishComplaint.php"
    public static var imageUploadUrl = BaseUrl+"imageupload.php"
    public static var downloadImageUrl = BaseUrl+"images/"
    public static var garbageUrl = BaseUrl+"showAllGarabageType.php"
    public static var getAllComplaintsCountUrl = BaseUrl+"showAllCountComplaints.php"
    public static var getAllPendingCountUrl = BaseUrl+"showAllCountofPendingComplaints.php"
    public static var getAllClosedCountUrl = BaseUrl+"showCountAllFinishComplaint.php"
    public static var profileUpdate = BaseUrl+"updateProfile.php"
    public static var sendFeedback = BaseUrl+"Feedback.php"
    public static var updateToken = BaseUrl+"updateToken.php"
    public static var showAllNotifications = BaseUrl+"showAllNotifications.php"
    public static var showComplaints = BaseUrl+"showComplaints.php"
    public static var showCountComplaints = BaseUrl+"showCountsComplaints.php"
    public static var closeComplaint = BaseUrl+"closeComplaint.php"
    public static var deleteComplaint = BaseUrl+"deleteComplaint.php"
    public static var insertFeedback = BaseUrl+"insertFeedback.php"
    public static var showfeedback = BaseUrl+"showfeedback.php"
    public static var getaddress = BaseUrl+"getaddress.php"

    //showfeedback.php
    //UC Complaints
//    public static var getAllUCComplaintsUrl = BaseUrl+"showAllUCComplaints.php"
//    public static var getAllPendingUCComplaints = BaseUrl+"showAllPendingUCComplaints.php"
//    public static var getAllProcessingUCComplaints = BaseUrl+"showAllProcessUCComplaints.php"
//    public static var getAllClosedUCComplaints = BaseUrl+"showAllFinishUCComplaints.php"
    public static var ComplaintPost = BaseUrl+"insertResponse.php"
    public static var ViewComplaints = BaseUrl+"ViewDetailsComplaints.php"
    
    // UC Counts
    public static var getAllUCCountComplaints = BaseUrl+"showAllUCCountComplaints.php"
    public static var getAllCountPendingUCComplaints = BaseUrl+"showAllCountPendingUCComplaints.php"
    public static var getCountAllFinishUCComplaint = BaseUrl+"showCountAllFinishUCComplaint.php"
    public static var getCountAllProcessUCComplaint = BaseUrl+"showCountAllProcessUCComplaint.php"
    
    
    
  //  init(BaseUrl: String) {
  //      self.BaseUrl = BaseUrl
  //  }
    
   public static func stopAllSessions() {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel()
                
            }
            
        }
        
    }

    public static func toastMessage(_ message: String){
            guard let window = UIApplication.shared.keyWindow else {return}
            let messageLbl = UILabel()
            messageLbl.text = message
            messageLbl.textAlignment = .center
            // messageLbl.font = UIFont.systemFont(ofSize: 13)
            messageLbl.font = UIFont(name: "Montserrat-Light", size: 12.0)
            messageLbl.textColor = .white
            messageLbl.backgroundColor = UIColor(white: 0, alpha: 0.8)
            let textSize:CGSize = messageLbl.intrinsicContentSize
            let labelWidth = min(textSize.width, window.frame.width - 40)
            messageLbl.frame = CGRect(x: 20, y: window.frame.height - 135, width: labelWidth + 20, height: textSize.height + 8)
            messageLbl.center.x = window.center.x
            messageLbl.layer.cornerRadius = messageLbl.frame.height/2
            messageLbl.layer.masksToBounds = true
            window.addSubview(messageLbl)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                UIView.animate(withDuration: 0.9, animations: {
                    messageLbl.alpha = 0
                }) { (_) in
                    messageLbl.removeFromSuperview()
                }
            }
        }
}


class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
