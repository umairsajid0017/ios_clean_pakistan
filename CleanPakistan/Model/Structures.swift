//
//  Structures.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 12/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation


import UIKit


struct SideMenu {
    var sideIcon:UIImage
    var sideTitle:String
}

struct Complain {
    var complainImage:String
    var complainColor:String
    var complainTitle:String
}

struct Areas {
    var id : String
    var area_name : String
    var uc_id : String
    var sm_email : String
   // var district_name : String
   // var union_concol_name : String
  //  var city_name : String
}

struct Garbage {
    var id: String
    var garbageName:String
    var garbageImage:String
}

struct Feedback {
    var responded: String
    var rating:String
    var current_date:String
    var complaint_id:String
    var comment:String
    var satisfy:String
}
