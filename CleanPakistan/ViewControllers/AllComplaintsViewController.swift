//
//  AllComplaintsViewController.swift
//  CleanPakistan
//
//  Created by Apple on 30/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import MapKit

var lat = String()
var long = String()
var Garbage_id: String = ""
var ComplaintID: String = ""
class AllComplaintsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    var ComplaintType = [String]()
    var City = [String]()
    var Address = [String]()
    var Area = [String]()
    var Status = [String]()
    var Photos = [String]()
    var Images = [UIImage]()
    var Location = [String]()
    var Complaint_Id = [String]()
    var Status_Id = [String]()
    var Date = [String]()
    var Customer_Remarks = [String]()
    var Employee_Remarks = [String]()
    var Posted_Date = [String]()
    var Processing_Date = [String]()
    var Closed_Date = [String]()
    var IsRespond = [String]()
    var IsDropped = [String]()
    
    //Button Views
    @IBOutlet weak var DirectionView: UIView!
    
    
    
   // var refreshControl = UIRefreshControl()
    var refreshControl: UIRefreshControl!
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var noComplaintsLbl: UILabel!
    @IBOutlet weak var ComplaintsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noComplaintsLbl.isHidden = true
        
        getAllComplaints()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        ComplaintsTableView.addSubview(refreshControl)
        
        // for navigation hide
    //    self.navigationController?.hidesBarsOnSwipe = true
        
    }
    override func viewDidAppear(_ animated: Bool) {
     //   SVProgressHUD.show(withStatus: "Please Wait")
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        SVProgressHUD.dismiss()
      //  AppConfig.stopAllSessions()
    }
    
    var refreshFlag : Bool = false
    
    @objc func refresh(sender:AnyObject) {
        
        print("Refresh")
        refreshFlag = true
        getAllComplaints()
        
        
        // Code to refresh table view
    }
    
    func clearTable()
    {
        self.ComplaintType.removeAll()
        self.City.removeAll()
        self.Address.removeAll()
        self.Area.removeAll()
        self.Status.removeAll()
        self.Photos.removeAll()
        self.Location.removeAll()
        self.Complaint_Id.removeAll()
        self.Status_Id.removeAll()
        self.Customer_Remarks.removeAll()
        self.Employee_Remarks.removeAll()
        self.Posted_Date.removeAll()
        self.Processing_Date.removeAll()
        self.ComplaintsTableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ComplaintType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllComplaintsTableViewCell", for: indexPath) as! AllComplaintsTableViewCell

        //Close Button
       // cell.closeButton.isHidden = true
        cell.RespondView.isHidden = true
        cell.FeedbackView.isHidden = true
        cell.DeleteView.isHidden = true
        cell.CloseView.isHidden = true
        cell.DirectionView.isHidden = true

        // Direction View
        cell.viewButton.tag = indexPath.row
        cell.viewButton.addTarget(self, action: #selector(viewSelected), for: .touchUpInside)
        
        cell.directionButton.tag = indexPath.row
        cell.directionButton.addTarget(self, action: #selector(directionSelected), for: .touchUpInside)
        
        cell.UcDirectionButton.tag = indexPath.row
        cell.UcDirectionButton.addTarget(self, action: #selector(UcDirectionSelected), for: .touchUpInside)
        
        // Close View
        cell.C_ViewButton.tag = indexPath.row
        cell.C_ViewButton.addTarget(self, action: #selector(C_ViewButtonSelected), for: .touchUpInside)
        
        cell.C_DirectionButton.tag = indexPath.row
        cell.C_DirectionButton.addTarget(self, action: #selector(C_DirectionButtonSelected), for: .touchUpInside)
        
        cell.closeButton.tag = indexPath.row
        cell.closeButton.addTarget(self, action: #selector(closeButtonSelected), for: .touchUpInside)
        
        // Delete View
        cell.D_DirectionButton.tag = indexPath.row
        cell.D_DirectionButton.addTarget(self, action: #selector(D_DirectionButtonSelected), for: .touchUpInside)
        
        cell.D_ViewButton.tag = indexPath.row
        cell.D_ViewButton.addTarget(self, action: #selector(D_ViewButtonSelected), for: .touchUpInside)
        
        cell.DeleteButton.tag = indexPath.row
        cell.DeleteButton.addTarget(self, action: #selector(DeleteButtonSelected), for: .touchUpInside)
        
        //Respond View
        cell.R_DirectionButton.tag = indexPath.row
        cell.R_DirectionButton.addTarget(self, action: #selector(R_DirectionButtonSelected), for: .touchUpInside)
        
        cell.R_ViewButton.tag = indexPath.row
        cell.R_ViewButton.addTarget(self, action: #selector(R_ViewButtonSelected), for: .touchUpInside)
        
        cell.respondButton.tag = indexPath.row
        cell.respondButton.addTarget(self, action: #selector(respondSelected), for: .touchUpInside)
        
        //Feedback
        cell.F_DirectionButton.tag = indexPath.row
        cell.F_DirectionButton.addTarget(self, action: #selector(F_DirectionButtonSelected), for: .touchUpInside)
        
        cell.F_ViewButton.tag = indexPath.row
        cell.F_ViewButton.addTarget(self, action: #selector(F_ViewButtonSelected), for: .touchUpInside)
        
        cell.FeedbackButton.tag = indexPath.row
        cell.FeedbackButton.addTarget(self, action: #selector(feedbackSelected), for: .touchUpInside)
        
        cell.statusIdLbl?.text = Status_Id[indexPath.row]
        
        // 1 pending
        // 2 closed
        // 4 processing
        
        //Area Manager
        if cell.statusIdLbl.text == "1" && user_role == "1" // pending
        {
            cell.DirectionView.isHidden = false
            cell.remarksLbl?.text! = Customer_Remarks[indexPath.row]
            cell.dateLBL?.text! = Posted_Date[indexPath.row]
        }
        else if cell.statusIdLbl.text == "2" && user_role == "1" // closed
        {
            cell.DirectionView.isHidden = false
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.dateLBL?.text! = Closed_Date[indexPath.row]
        }
        else if cell.statusIdLbl.text == "4" && user_role == "1" // processing
        {
            cell.CloseView.isHidden = false
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.dateLBL?.text! = Closed_Date[indexPath.row]
            cell.dateLBL?.text! = Processing_Date[indexPath.row]
        }
            
        //User
        else if cell.statusIdLbl.text == "1" && user_role == "0" //pending
        {
            if(IsDropped[indexPath.row] == "0")
            {
                cell.DeleteView.isHidden = false
            }
            else
            {
                cell.DirectionView.isHidden = false
            }
            
            
            cell.remarksLbl?.text! = Customer_Remarks[indexPath.row]
            cell.dateLBL?.text! = Posted_Date[indexPath.row]
        }
        else if cell.statusIdLbl.text == "2" && user_role == "0" // closed
        {
            if(IsRespond[indexPath.row] == "0")
            {
                cell.FeedbackView.isHidden = false
            }
            else
            {
                cell.DirectionView.isHidden = false
            }
            
            
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.dateLBL?.text! = Closed_Date[indexPath.row]
        }
        else if cell.statusIdLbl.text == "4" && user_role == "0" //processing
        {
            cell.DirectionView.isHidden = false
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.dateLBL?.text! = Processing_Date[indexPath.row]
        }
        
        // District Manager
        if cell.statusIdLbl.text == "1" && user_role == "2" // pending
        {
            cell.DirectionView.isHidden = false
            cell.remarksLbl?.text! = Customer_Remarks[indexPath.row]
            cell.dateLBL?.text! = Posted_Date[indexPath.row]
        }
        else if cell.statusIdLbl.text == "2" && user_role == "2" // closed
        {
            cell.DirectionView.isHidden = false
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.dateLBL?.text! = Closed_Date[indexPath.row]
        }
        else if cell.statusIdLbl.text == "4" && user_role == "2" // processing
        {
            cell.DirectionView.isHidden = false
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.dateLBL?.text! = Closed_Date[indexPath.row]
            cell.dateLBL?.text! = Processing_Date[indexPath.row]
        }
        
        // Social Monitor
        if cell.statusIdLbl.text == "1" && user_role == "3" // pending
        {
            cell.RespondView.isHidden = false
            cell.remarksLbl?.text! = Customer_Remarks[indexPath.row]
            cell.dateLBL?.text! = Posted_Date[indexPath.row]
        }
        else if cell.statusIdLbl.text == "2" && user_role == "3" // closed
        {
            cell.DirectionView.isHidden = false
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.dateLBL?.text! = Closed_Date[indexPath.row]
        }
        else if cell.statusIdLbl.text == "4" && user_role == "3" // processing
        {
            cell.DirectionView.isHidden = false
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.dateLBL?.text! = Closed_Date[indexPath.row]
            cell.dateLBL?.text! = Processing_Date[indexPath.row]
        }
        
        
        cell.garbagelbl?.text! = ComplaintType[indexPath.row]
        cell.garbagelbl.numberOfLines = 0;
        cell.garbagelbl.lineBreakMode = .byWordWrapping
        
        cell.addressLbl?.text! = Address[indexPath.row]
        cell.addressLbl.numberOfLines = 0;
        cell.addressLbl.lineBreakMode = .byWordWrapping
        
        cell.remarksLbl.numberOfLines = 0;
        cell.remarksLbl.lineBreakMode = .byWordWrapping
        
        cell.areaLbl?.text! = Area[indexPath.row]
        cell.cityLbl.text = City[indexPath.row]
        cell.statusLbl?.text! = Status[indexPath.row]
        cell.locationLbl?.text! = Location[indexPath.row]
        cell.locationLbl.isHidden = true
        cell.idLbl?.text! = Complaint_Id[indexPath.row]
        cell.idLbl.isHidden = true
        cell.statusIdLbl.isHidden = true
        let imageName = Photos[indexPath.row]
        
        let remoteImageURL = URL(string: AppConfig.downloadImageUrl+imageName)!
        
        // Use Alamofire to download the image
        Alamofire.request(remoteImageURL).responseData { response in
            guard let image = UIImage(data:response.data!) else {
                // Handle error
                print("No Image")
                return
            }
           guard let imageData = image.jpegData(compressionQuality: 0.5) else{
            return
            }
            cell.complaintImage.image = UIImage(data : imageData)
            cell.complaintImage.layer.masksToBounds = true
            cell.complaintImage.layer.cornerRadius = 4.0;
           // cell.complaintImage.contentMode = UIView.ContentMode.scaleAspectFit
            
        }
        return cell
    }

    

    //direction
    @objc func directionSelected(sender: UIButton){
       
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl?.text!
      //  print(locationData)
        
        let data = locationData!.split(separator: ",")
        
        lat = String(data[0])
        long = String(data[1])
        
        print("latitude = \(lat) and longitude = \(long)")
        
        
        //Defining destination
        let latitude:CLLocationDegrees = CLLocationDegrees(lat)!
        let longitude:CLLocationDegrees = CLLocationDegrees(long)!
        
        let regionDistance:CLLocationDistance = 1000;
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = cell.addressLbl.text!
        mapItem.openInMaps(launchOptions: options)
    }
    
    //closeButtonSelected
    @objc func closeButtonSelected(sender: UIButton){
        
        let alertController = UIAlertController(title: "Close this complaint", message: "Are you sure? After closing this complaint, user will get notification", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            print("Call Pressed")
            print(sender.tag)
            let tag:NSInteger = sender.tag
            let indexPath = IndexPath(row: tag, section: 0)
            print(indexPath)
            
            let id = self.Complaint_Id[indexPath.row]
            self.closeComplaints(Comp_id: id)
            
            
        }
        let NoAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            
            print("Call Pressed")
            //  self.present(alertController, animated: true, completion: nil)
            
            
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(NoAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //DeleteButtonSelected
    @objc func DeleteButtonSelected(sender: UIButton){
        
        
        let alertController = UIAlertController(title: "Drop this complaint", message: "Are you sure? After dropping this complaint, no action will be done on this complaint", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            print("Call Pressed")
            print(sender.tag)
            let tag:NSInteger = sender.tag
            let indexPath = IndexPath(row: tag, section: 0)
            print(indexPath)
            
            let id = self.Complaint_Id[indexPath.row]
            self.deleteComplaints(Comp_id: id)
         //   self.present(alertController, animated: true, completion: nil)
            
            
        }
        let NoAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            
            print("Call Pressed")
          //  self.present(alertController, animated: true, completion: nil)
            
            
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(NoAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        ////////////////////////////
       
        
        
        
    }

    // direction
    @objc func UcDirectionSelected(sender: UIButton){
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        let locationName = cell.addressLbl.text!
        directionCall(locationData: locationData, name: locationName)
    }
    //close direction
    @objc func C_DirectionButtonSelected(sender: UIButton){
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        let locationName = cell.addressLbl.text!
        directionCall(locationData: locationData, name: locationName)
    }
    //delete direction
    @objc func D_DirectionButtonSelected(sender: UIButton){
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        let locationName = cell.addressLbl.text!
        directionCall(locationData: locationData, name: locationName)
    }
    //respond direction
    @objc func R_DirectionButtonSelected(sender: UIButton){
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        let locationName = cell.addressLbl.text!
        directionCall(locationData: locationData, name: locationName)
    }
    // feedback direction
    @objc func F_DirectionButtonSelected(sender: UIButton){
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        let locationName = cell.addressLbl.text!
        directionCall(locationData: locationData, name: locationName)
    }
    
    
    func directionCall(locationData: String, name: String)
    {
        print(locationData)
        
        let data = locationData.split(separator: ",")
        
        lat = String(data[0])
        long = String(data[1])
        
        print("latitude = \(lat) and longitude = \(long)")
        
        //Defining destination
        let latitude:CLLocationDegrees = CLLocationDegrees(lat)!
        let longitude:CLLocationDegrees = CLLocationDegrees(long)!
        
        let regionDistance:CLLocationDistance = 1000;
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)
    }
    //respond
    @objc func respondSelected(sender: UIButton){
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        //Bug
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        Garbage_id = ""
        Garbage_id = cell.idLbl.text!
        print(Garbage_id)
        print(locationData)
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
    }
    // view
    @objc func viewSelected(sender: UIButton){
        
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        Garbage_id = ""
        Garbage_id = cell.idLbl.text!
        print(Garbage_id)
        print(locationData)
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ViewComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
    }
    // Close view
    @objc func C_ViewButtonSelected(sender: UIButton){
        
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        Garbage_id = ""
        Garbage_id = cell.idLbl.text!
        print(Garbage_id)
        print(locationData)
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ViewComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
    }
    // Delete view
    @objc func D_ViewButtonSelected(sender: UIButton){
        
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        Garbage_id = ""
        Garbage_id = cell.idLbl.text!
        print(Garbage_id)
        print(locationData)
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ViewComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
    }
    // Respond view
    @objc func R_ViewButtonSelected(sender: UIButton){
        
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        Garbage_id = ""
        Garbage_id = cell.idLbl.text!
        print(Garbage_id)
        print(locationData)
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ViewComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
    }
    // Feedback
    @objc func feedbackSelected(sender: UIButton){
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        ComplaintID = Complaint_Id[indexPath.row]
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
    }
    // Feedback view
    @objc func F_ViewButtonSelected(sender: UIButton){
        
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = ComplaintsTableView.cellForRow(at: indexPath) as! AllComplaintsTableViewCell
        let locationData = cell.locationLbl.text!
        Garbage_id = ""
        Garbage_id = cell.idLbl.text!
        print(Garbage_id)
        print(locationData)
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ViewComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
    }
    
    //Close Complaint
    func closeComplaints(Comp_id: String) {
        let url = AppConfig.closeComplaint
        
        let email = UserDefaults.standard.value(forKey: "Email") as! String
        
        let parameters = ["email": email,"comp_id": Comp_id] as [String : Any]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success:
                print(response)
                
                self.refreshFlag = true
                self.getAllComplaints()
                
                break
                
            case .failure:
                print("error")
            }
        }
    }
    
    //Delete Complaint
    func deleteComplaints(Comp_id: String) {
        
        let url = AppConfig.closeComplaint
        
        let email = UserDefaults.standard.value(forKey: "Email") as! String
        
        let parameters = ["email": email,"comp_id": Comp_id] as [String : Any]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success:
                print(response)
                
                self.refreshFlag = true
                self.getAllComplaints()
                
                break
                
            case .failure:
                print("error")
            }
        }
    }
    
    
    func getAllComplaints(){
        AppConfig.stopAllSessions()
        
        if(!refreshFlag)
        {
            SVProgressHUD.show(withStatus: "Please Wait")
        }
        
     //   var url = String()
        
    //    var parameters = [String: String]()
        var status: String = ""
        
        
        if ComplaintCategory == "AllComplaints"
        {
            status = "-1"
//            url = AppConfig.getAllComplaintsUrl
        }
        else if ComplaintCategory == "AllUcComplaints"
        {
            status = "-1"
//            url = AppConfig.getAllUCComplaintsUrl
        }
        else if ComplaintCategory == "PendingComplaints"
        {
            status = "1"
//            if user_role == "0"
//            {
//                url = AppConfig.getAllPendingUrl
//            }
//            else if user_role == "1"
//            {
//                url = AppConfig.getAllPendingUCComplaints
//            }
            
        }
        else if ComplaintCategory == "ProcessingComplaints"
        {
            status = "4"
//            url = AppConfig.getAllProcessingUCComplaints
        }
        else if ComplaintCategory == "ClosedComplaints"
        {
            status = "2"
//            if user_role == "0"
//            {
//                url = AppConfig.getAllClosedUrl
//            }
//            else if user_role == "1"
//            {
//                url = AppConfig.getAllClosedUCComplaints
//            }
        }
        else if ComplaintCategory == "DroppedFeedback"
        {
            status = "-3"
            //            url = AppConfig.getAllProcessingUCComplaints
        }
        else if ComplaintCategory == "PendingFeedback"
        {
            status = "-2"
            //            url = AppConfig.getAllProcessingUCComplaints
        }
        
        
//        if user_role == "1"
//        {
//            print(url)
//            let uc_area_id = UserDefaults.standard.value(forKey: "Area_Id") as! String
//            print(uc_area_id)
//            parameters = ["uc_area_id": uc_area_id] //as! [String : String]
//        }
//        else if user_role == "0"
//        {
//            print(url)
//            let userEmail = UserDefaults.standard.value(forKey: "Email") as! String
//            parameters = ["user_email": userEmail] //as! [String : String]
//        }
        
        
        let url = AppConfig.showComplaints
        
        let userEmail = UserDefaults.standard.value(forKey: "Email") as! String
        
        let parameters = ["user_email": userEmail, "status": status]
        
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result){
                case .success:
                    if let json = response.result.value{
                        self.clearTable()
                        let JSON = json as! NSDictionary
                        print(JSON.allKeys)
                        print(JSON)
                        for res in JSON{
                            if ("\(res.key)" == "records")
                            {
                                record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                for innerResult in record{
                                    // print(innerResult)
                                    
                                    let dicFrmArray = innerResult
                                    
                                    if ((dicFrmArray["garabage_name"] as? NSNull) == nil && dicFrmArray["city_name"] != nil && dicFrmArray["address"] != nil && dicFrmArray["area_name"] != nil && dicFrmArray["status_name"] != nil && dicFrmArray["photo"] != nil && dicFrmArray["location"] != nil && dicFrmArray["id"] != nil && dicFrmArray["status_id"] != nil && dicFrmArray["emp_remarks"] != nil && dicFrmArray["customer_remark"] != nil && dicFrmArray["posted_at"] != nil && dicFrmArray["processed_date"] != nil && dicFrmArray["closed_date"] != nil && dicFrmArray["dropped"] != nil && dicFrmArray["isresponsed"] != nil){
                                        
                                        let Garbage = (dicFrmArray["garabage_name"])
                                        let City_Name = (dicFrmArray["city_name"] )
                                        let Address_ = (dicFrmArray["address"] )
                                        let Area_Name = (dicFrmArray["area_name"] )
                                        let Status_Name = (dicFrmArray["status_name"] )
                                        let photos = (dicFrmArray["photo"] )
                                        let Location = (dicFrmArray["location"])
                                        let ComplaintId = (dicFrmArray["id"])
                                        let StatusId = (dicFrmArray["status_id"])
                                       // let EmployeeRemarks = (dicFrmArray["emp_remarks"])
                                        let CustomerRemarks = (dicFrmArray["customer_remark"])
                                        let PostedDate = (dicFrmArray["posted_at"])
                                        
                                        let ProcessingDate = (dicFrmArray["processed_date"])
                                        let Dropped = (dicFrmArray["dropped"])
                                        let Responsed = (dicFrmArray["isresponsed"])
                                       // let ClosedDate = (dicFrmArray["closed_date"])
                                        
                                        var EmployeeRemarks: String = ""
                                        
                                        if (dicFrmArray["emp_remarks"] as? NSNull)  == nil
  
                                        {
                                            EmployeeRemarks = (dicFrmArray["emp_remarks"]) as! String
                                        }
                                        else
                                        {
                                            EmployeeRemarks = ""
                                        }
                                        
                                        
                                        var closedDate: String = "00-00-00"

                                        
                                        
                                        if (dicFrmArray["closed_date"] as? NSNull)  == nil
                                        {
                                            closedDate = (dicFrmArray["closed_date"]) as! String
                                            
                                        }
                                        else
                                        {
                                            closedDate = "00-00-00"
                                        }
                                        
                                        
                                        print(ComplaintId as! String)
                                        //status_id
                                        self.ComplaintType.append(Garbage as! String)
                                        self.City.append(City_Name as! String)
                                        self.Address.append(Address_ as! String)
                                        self.Area.append(Area_Name as! String)
                                        self.Status.append(Status_Name as! String)
                                        self.Photos.append(photos as! String)
                                        self.Location.append(Location as! String)
                                        self.Complaint_Id.append(ComplaintId as! String)
                                        self.Status_Id.append(StatusId as! String)
                                        self.Customer_Remarks.append(CustomerRemarks as! String)
                                        self.Employee_Remarks.append(EmployeeRemarks)
                                        self.Posted_Date.append(PostedDate as! String)
                                        self.Processing_Date.append(ProcessingDate as! String)
                                        
                                        self.IsRespond.append(Responsed as! String)
                                        self.IsDropped.append(Dropped as! String)
                                        
                                        self.Closed_Date.append(closedDate)
                                        
                                        
                                    }
                                    
                                }
                            //    for data in self.Closed_Date
                            //    {
                            //        print(data)
                            //    }
                                
                                self.ComplaintsTableView.reloadData()
                                self.refreshControl.endRefreshing()
                                if(!self.refreshFlag)
                                {
                                    SVProgressHUD.dismiss()
                                    
                                }
                           //     for data in self.Complaint_Id
                           //     {
                           //         print(data)
                           //     }
                            }
                        }
                        
                    }
                    else{
                        print("Failure")
                           self.noComplaintsLbl.isHidden = false
                           self.ComplaintsTableView.isHidden = true
                    }
                    break
                case .failure:
                    if let json = response.result.value{
                        print(json)
                           self.noComplaintsLbl.isHidden = false
                           self.ComplaintsTableView.isHidden = true
                        if(!self.refreshFlag)
                        {
                            SVProgressHUD.dismiss()

                        }
                    }
                    break
                }
        }
        
    }
    
}

