
//  FeedbackViewController.swift
//  CleanPakistan
//
//  Created by Apple on 28/12/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON

class FeedbackViewController: UIViewController, UITextViewDelegate {

    var userRating : Double = 0
    var Resolved = "1"
    var Respond = "1"
    var Satisfied = "1"
    
    @IBOutlet weak var feedbackView: UIView!
    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var resolvedSegment: UISegmentedControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  var view = CosmosView()
        ratingView.settings.emptyColor = .lightGray
        ratingView.settings.fillMode = .precise
        // Change the cosmos view rating
        ratingView.rating = 0
        /*
        // Do not change rating when touched
        // Use if you need just to show the stars without getting user's input
        ratingView.settings.updateOnTouch = false
        */
        // Called when user finishes changing the rating by lifting the finger from the view.
        // This may be a good place to save the rating in the database or send to the server.
        ratingView.didFinishTouchingCosmos = { rating in
            print(rating)
            self.userRating = rating
        }
        
        
        // A closure that is called when user changes the rating by touching the view.
        // This can be used to update UI as the rating is being changed by moving a finger.
        ratingView.didTouchCosmos = { rating in
            print(rating)
            self.userRating = rating
        }
        
        
        feedbackTextView.delegate = self
        feedbackTextView.text = "Please write feedback here..."
        feedbackTextView.textColor = UIColor.lightGray
        
        // Do any additional setup after loading the view.
    }
    

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please write feedback here..."
            textView.textColor = UIColor.lightGray
        }
    }

    @IBAction func resolutionAction(_ sender: UISegmentedControl) {
        
        print ("index: ", sender.selectedSegmentIndex)
        
        if sender.selectedSegmentIndex == 0 {
            //print("Resolved")
            Resolved = "1"
            print(Resolved)
        }
        
        if sender.selectedSegmentIndex == 1 {
            //print("Partially Resolved ")
            Resolved = "2"
            print(Resolved)
        }
        
        
        if sender.selectedSegmentIndex == 2 {
           // print("Not Resolved")
            Resolved = "3"
            print(Resolved)
        }
        
    }
    
    @IBAction func respondAction(_ sender: UISegmentedControl) {
        
        print ("index: ", sender.selectedSegmentIndex)
        
        if sender.selectedSegmentIndex == 0 {
           // print("Yes")
            Respond = "1"
            print(Respond)
        }
        
        if sender.selectedSegmentIndex == 1 {
           // print("No")
            Respond = "2"
            print(Respond)
            
        }
        
    }
    
    @IBAction func satisfiedAction(_ sender: UISegmentedControl) {
        
        print ("index: ", sender.selectedSegmentIndex)
        
        if sender.selectedSegmentIndex == 0 {
            Satisfied = "1"
            print(Satisfied)
        }
        
        if sender.selectedSegmentIndex == 1 {
            Satisfied = "2"
            print(Satisfied)
            
        }
    }
    
    @IBAction func submitAction(_ sender: Any) {
        /*
        responded:1
        rating:3.5
        complaint_id: 14
        comment: Work done better
        satisfy:1
        resolution_id:2
        */
        if(feedbackTextView.text! == "Please write feedback here..." || feedbackTextView.text! == "")
        {
            let msg = "Please write feedback"
            AppConfig.toastMessage(msg)
        }
        else
        {
        
        let Coment = feedbackTextView.text!
        InsertFeedback(respond: Respond, rating: userRating, complaint_id: ComplaintID, comet: Coment, satisfy: Satisfied, resolution: Resolved)
        }
    }
    
    //Insert Feedback
    func InsertFeedback(respond: String, rating: Double,complaint_id:String, comet: String, satisfy: String, resolution: String) {
        let url = AppConfig.insertFeedback
        
        let parameters = ["responded": respond,"rating": rating,"complaint_id" : complaint_id,"comment" : comet,"satisfy" : satisfy, "resolution_id" : resolution] as [String : Any]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success:
                print(response)
                
                let a = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController")
                self.navigationController?.pushViewController(a!, animated: true)
               // self.dismiss(animated: true, completion: nil)
                
                break
                
            case .failure:
                print("error")
            }
        }
    }
}
