//
//  SideMenuViewController.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 12/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


class SideMenuViewController: BaseViewController {
    
    
    @IBOutlet weak var tableView:UITableView!
    
    @IBOutlet weak var userEmailLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userTypeLbl: UILabel!
    @IBOutlet weak var imgView: RoundedImageView!
    @IBOutlet weak var userTypeView: UIView!
    
    var imageName : String = "placeholder_user.png"
    var setImage = "1"
    // let arraySideMenu = ["","Goals Archieve", "Profile", "LogOut"]
    
    
    var sideMenu : [SideMenu] = [SideMenu]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userTypeView.layer.cornerRadius = 12
        userTypeView.clipsToBounds = true
        
        
        let userRole = UserDefaults.standard.value(forKey: "role") as! String
        if (userRole == "0")
        {
            userTypeLbl.text = "User"
        }
        else if (userRole == "1")
        {
            userTypeLbl.text = "Area Manager"
        }
        else if (userRole == "2")
        {
            userTypeLbl.text = "District Manager"
        }
        else if (userRole == "3")
        {
            userTypeLbl.text = "Social Monitor"
        }
        
        userEmailLbl.text! = UserDefaults.standard.value(forKey: "Email") as! String
        userNameLbl.text! = UserDefaults.standard.value(forKey: "Name") as! String
        
        sideMenu.append(SideMenu(sideIcon: UIImage(named: "user-1.png")!, sideTitle: "Edit Profile"))
        
        //logout
        sideMenu.append(SideMenu(sideIcon: UIImage(named: "feedback-1.png")!, sideTitle: "Contact Us"))
        sideMenu.append(SideMenu(sideIcon: UIImage(named: "star-1.png")!, sideTitle: "Rate Us"))
        sideMenu.append(SideMenu(sideIcon: UIImage(named: "logout.png")!, sideTitle: "Logout"))
        let PhotoName = UserDefaults.standard.value(forKey: "Image") as! String
        
        if PhotoName == ""
        {
            getImage(name: imageName)
        }
        else
        {
            getImage(name: PhotoName)
        }
        
        
        self.tableView.reloadData()
        
        self.tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(setImage == "1")
        {
            let PhotoName = UserDefaults.standard.value(forKey: "Image") as! String
            if PhotoName == ""
            {
                getImage(name: imageName)
            }
            else
            {
                getImage(name: PhotoName)
            }
        }
        
    }
    // get Image
    
    func getImage(name: String){
        
        let ImageName = name
        print(ImageName)
        let remoteImageURLBefore = URL(string: AppConfig.downloadImageUrl+ImageName)!
        
        // Use Alamofire to download the image
        Alamofire.request(remoteImageURLBefore).responseData { response in
            guard let image = UIImage(data:response.data!) else {
                // Handle error
                print("No Image")
                return
            }
            guard let imageData = image.jpegData(compressionQuality: 0.3) else{
                return
            }
            self.imgView.image = UIImage(data : imageData)
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension SideMenuViewController :UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        
        let sideMenu = self.sideMenu[indexPath.row]
        
        cell.sideIcon.image = sideMenu.sideIcon
        cell.sideTitle.text = sideMenu.sideTitle
        
      //  cell.sideMenuLabel.text = self.arraySideMenu[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        if indexPath.row == 0{
            if revealViewController()!.isLeftViewOpen {
                revealViewController()?.hideLeftView(animated: true)
            }
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "EditProfileViewController")
            self.show(vc, sender: nil)
            
   
        }



        else if indexPath.row == 1{
            if revealViewController()!.isLeftViewOpen {
                revealViewController()?.hideLeftView(animated: true)
            }
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ContactUsViewController")
            self.show(vc, sender: nil)
          
        }
            
        else if indexPath.row == 3{
            /*
            if revealViewController()!.isLeftViewOpen {
                revealViewController()?.hideLeftView(animated: true)
            }
            */
            let name = UserDefaults.standard.value(forKey: "Name") as! String
            
            let alertController = UIAlertController(title: "Do you want to logout as "+name+"?", message: "", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "YES LOGOUT", style: UIAlertAction.Style.default) {
                UIAlertAction in
                
                print("Call Pressed")
                print("Logout")

                AppConfig.stopAllSessions()
                self.setImage = "2"
                let defaults = UserDefaults.standard
                let dictionary = defaults.dictionaryRepresentation()
                
                dictionary.keys.forEach
                    { key in   defaults.removeObject(forKey: key)
                }
                
                let a = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
                self.present(a!, animated: true, completion: nil)
                
                
            }
            let NoAction = UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                
                print("Call Pressed")
                //  self.present(alertController, animated: true, completion: nil)
                
            }
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(NoAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
            
        }

        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
