//
//  CreatePostViewController.swift
//  CleanPakistan
//
//  Created by Apple on 04/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MobileCoreServices
import AVKit
import SVProgressHUD
import CoreLocation


class CreatePostViewController: UIViewController,UIImagePickerControllerDelegate,CLLocationManagerDelegate, UINavigationControllerDelegate, UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {
    
   
    
    let locationManager = CLLocationManager()
    
    var LocationAddress = ""

    var i = 1
   // var locationManager : CLLocationManager! = CLLocationManager();
    
  //  var veryCurrentLocation : CLLocation! = nil;
    
    var areasArray = [String]()

   // var locationCordinates = CLLocationCoordinate2D()
    var lat = CLLocationDegrees()
    var lng = CLLocationDegrees()
    
    var areaData = [Areas]()
    var imageName : String = "placeholder.png"
    var locationFill : String = ""
    var areaFill : String = ""
    var current_street : String = ""
    var current_city : String = ""
    var current_country : String = ""
    
    @IBOutlet weak var remarksTextField: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var responsdBtn: UIButton!
    @IBOutlet weak var postBtn: UIButton!
    
    
    var image_Picker = UIImagePickerController()
    
    @IBOutlet weak var areasTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if user_role == "0"
        {
            responsdBtn.isHidden = true
        }
        
        else if user_role == "1"
        {
            areaTextField.isHidden = true
            locationTextField.isHidden = true
            postBtn.isHidden = true
        }
        
        areasTableView.isHidden = true
      //  getAreasData(address: <#String#>)
        
        image_Picker.delegate = self
        areaTextField.delegate = self
     //   self.setupLocationManager()
        
        
        //locationManager.allowsBackgroundLocationUpdates = true
       // locationManager.pausesLocationUpdatesAutomatically = false
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("Text filed should begin editing")
        areasTableView.isHidden = false
        areasTableView.reloadData()
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return areaData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AreasTableViewCell", for: indexPath) as! AreasTableViewCell
        cell.areaNameLbl.text = areaData[indexPath.row].area_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        areaTextField.text! = areaData[indexPath.row].area_name
        areaFill = "1"
        areasTableView.isHidden = true

    }
    
    
    //end TableView Functions
    @IBAction func PostComplaintButton(_ sender: UIButton) {
       
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            if (locationTextField.text! == "" || remarksTextField.text! == "" || areaTextField.text! == "")
            {
                
                print("Fill all fields")
                let msg = "Please fill all fields"
                AppConfig.toastMessage(msg)
            }
            else
            {
                if (locationTextField.text! == "No detect pin location")
                {
                    let msg = "First select pin location"
                    AppConfig.toastMessage(msg)
                }
                else
                {
                    var UcId = String()
                    for data in areaData
                    {
                        if data.area_name == areaTextField.text
                        {
                            UcId = data.id
                        }
                    }
                    let GarbageType = garbageTypeId
                    let Email = UserDefaults.standard.value(forKey: "Email") as! String
                    let CustomerRemarks = remarksTextField.text!
                    let Address: String = locationTextField.text!
                    let Location = String(lat)+","+String(lng)
                    // print(Location)
                    let CityId = UserDefaults.standard.value(forKey: "City_Id") as! String
                    let AreaUcId = UcId
                    let image = imageName
                    print(image)
                    complaintPost(garbage_type: GarbageType, user_email: Email, Image: image, customer_remark: CustomerRemarks, address: Address, location: Location, city_id: CityId, uc_area_id: AreaUcId)
                    uploadImage(imgView.image)
                }
            }
        }
        else{
            let msg = "No Internet Connection"
            AppConfig.toastMessage(msg)
        }
    }
    
    
    @IBAction func respondButton(_ sender: Any) {
        
        if Connectivity.isConnectedToInternet() {
            if(remarksTextField.text! == "")
            {
                let msg = "Please fill all fields"
                AppConfig.toastMessage(msg)
            }else
            {
                let Email = UserDefaults.standard.value(forKey: "Email") as! String
                let Remarks = remarksTextField.text!
                let Img = imgView.image!
                let Id = Garbage_id
                let image = imageName
                createResponsed(email: Email, remarks: Remarks, imageAfter: image, id: Id)
                
                uploadImage(Img)
            }
        }else{
            let msg = "No Internet Connection"
            AppConfig.toastMessage(msg)
        }
    }
    
    
    
    @IBAction func takePhotoBtn(_ sender: UIButton) {
        
        openCamera()
        
    }
    
    func selectImageFrom(){
        image_Picker =  UIImagePickerController()
        image_Picker.delegate = self
       
        image_Picker.sourceType = .camera
        
        present(image_Picker, animated: true, completion: nil)
    }
    
    @IBAction func pickPhotoBtn(_ sender: UIButton) {
        let alert = UIAlertController(title: "Gallery Media", message: "Please Select A Photo", preferredStyle: .actionSheet);
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil);
        let photos = UIAlertAction(title: "Photos", style: .default, handler: {(alert : UIAlertAction) in
            self.chooseMedia(type: kUTTypeImage)
        })

        alert.addAction(photos);
        alert.addAction(cancel);
        present(alert, animated: true, completion: nil)
    }
    // Browse Profile Image Functions
    private func chooseMedia(type: CFString){
        image_Picker.mediaTypes = [type as String]
        present(image_Picker, animated: true, completion: nil)
    }
    
    private func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            image_Picker.sourceType = UIImagePickerController.SourceType.camera
            image_Picker.allowsEditing = true
            self.present(image_Picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageName = String.random() //UUID().uuidString
        print(imageName)
        imageName = imageName+".png"
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
           // selectedImage = editedImage
            imgView.image = image
            picker.dismiss(animated: true, completion: nil)
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
           // selectedImage = originalImage
            imgView.image = image
            //imgView.needsDisplay = true
            picker.dismiss(animated: true, completion: nil)
        } else {
            print("Something went wrong")
        }
    }// end Browse Profile Image Functions
    
    // get areas from Api
    func getAreasData(address: String){
        let url = AppConfig.getAllUCAreas
        let cityId = UserDefaults.standard.value(forKey:"City_Id") as! String
        print(cityId)
        let parameters = ["city_id": cityId]
        print(url)
        var record = [NSDictionary]()
        var flag = false
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch response.result{
                case .success:
                    if let json = response.result.value{
                        let JSON = json as! NSDictionary
                        print(JSON.allKeys)
                        print(JSON)
                        for res in JSON{
                            if ("\(res.key)" == "records")
                            {
                                record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                for innerResult in record{
                                    // print(innerResult)
                                    
                                    let dicFrmArray = innerResult
                                    
                                    if ((dicFrmArray["id"] as? NSNull) == nil && dicFrmArray["area_name"] != nil && dicFrmArray["uc_id"] != nil && dicFrmArray["sm_email"] != nil){
                                        
                                        let Id = (dicFrmArray["id"])
                                        let Area_name = (dicFrmArray["area_name"] ) as! String
                                        let Uc_id = (dicFrmArray["uc_id"] )
                                        let Sm_Email = (dicFrmArray["sm_email"] )
                                        
                                        flag = false
                                        let areaAddress = Area_name.split(separator: " ")
                                        for data in areaAddress
                                        {
                                            if(address.contains(data))
                                            {
                                                flag = true
                                            }
                                            else
                                            {
                                                flag = false
                                                break
                                            }
                                        
                                        }
                                        if(flag)
                                        {
                                            //Structure
                                            let data = Areas(id: Id as! String, area_name: Area_name , uc_id: Uc_id as! String, sm_email: Sm_Email as! String)
                                            self.areaData.append(data)
                                        }
                                        
                                        if(!flag)
                                        {
                                            let areaAddress = Area_name.split(separator: " ")
                                            for data in areaAddress
                                            {
                                                if(address.contains(data))
                                                {
                                                    let data = Areas(id: Id as! String, area_name: Area_name , uc_id: Uc_id as! String, sm_email: Sm_Email as! String)
                                                    self.areaData.append(data)
                                                }
                                                
                                                
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }
                        
                        for data in self.areaData
                        {
                            print(data.area_name)
                        }
                        
                        self.areasTableView.reloadData()
                        
                    }
                    else{
                        print("Failure")
                    }
                    break
                case .failure:
                    if let json = response.result.value{
                        print(json)
                    }
                    break
                }
        }
        
    }
    
   
    //RegisterComplaint
    func complaintPost(garbage_type: String, user_email: String,Image:String, customer_remark: String, address: String, location: String, city_id: String, uc_area_id: String) {
        let url = AppConfig.insertComplaints 
        
        print(imageName+" complainPost")
        
        print(city_id)
        print(uc_area_id)
        let parameters = ["garbage_type": garbage_type,"user_email": user_email,"photo" : Image,"customer_remark" : customer_remark,"address" : address,"location" : location,"city_id" : city_id,"uc_area_id" : uc_area_id] as [String : Any]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success( _):
                print(response)
                
               // self.dismiss(animated: true, completion: nil)
                
            case .failure( _):
                print("error")
            }
        }
    }
    
    //Image Upload
    func uploadImage(_ profileImage:UIImage?){
        
        SVProgressHUD.show(withStatus: "Upload Image")
        
        let url = AppConfig.imageUploadUrl
        
        guard let profileImage = profileImage else {
            return
        }
        
         guard let imageDATA = profileImage.jpegData(compressionQuality: 0.45) else { return }
        
        //let email = UserDefaults.standard.value(forKey: "Email")
       // let imageName = UUID().uuidString
      //  print(imageName)
        
        let imageData = imageDATA
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "image",fileName: "\(self.imageName)", mimeType: "image/png")
        },to:url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    
                    let with100  =  progress.fractionCompleted * 100
                    
                    let xx = Int(with100)
                    
                    print("Upload Progress With Int: \(xx)")
                    
                })
                
                upload.responseString{ response in
                  //  print(response.result.value)
                    
                    SVProgressHUD.dismiss()
                    print("Done With Uploading")
                    let a = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController")
                    self.navigationController?.pushViewController(a!, animated: true)
                  //  self.present(a!, animated: true, completion: nil)
                   // self.dismiss(animated: true, completion: nil)
                    //self..isEnabled = true
                    //   self.imageUploaded = true
                }
                
            case .failure(let encodingError):
                
                //  self.profileImage.image
                print(encodingError)
            }
        }
    }

    
    //createResponsed
    func createResponsed(email: String, remarks: String,imageAfter: String, id: String) {
        let url = AppConfig.ComplaintPost
        
        let parameters = ["emp_email": email,"emp_remarks": remarks,"photo_after": imageAfter,"id" : id]
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success( _):
                print(response)
                
               print("Sucessfull")
               SVProgressHUD.dismiss()
                break
                
            case .failure( _):
                print("error")
                SVProgressHUD.dismiss()
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        
        
        lat = coord.latitude
        lng = coord.longitude
        print(lat)
        print(lng)
        getLocation()
    }
    
    var street = ""
    var area = ""
    // get Address
    func getAddress(){
        AppConfig.stopAllSessions()
        
        
        let url = AppConfig.getaddress
        
        
        let parameters = ["lat": lat, "lng": lng]
        
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result){
            case .success:
                if let json = response.result.value{
                    let JSON = json as! NSDictionary
                    print(JSON.allKeys)
                    print(JSON)
                    for res in JSON{
                        if ("\(res.key)" == "records")
                        {
                            record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                            for innerResult in record{
                                // print(innerResult)
                                
                                let dicFrmArray = innerResult
                                
                                if ((dicFrmArray["address"] as? NSNull) == nil ){
                                    
                                    let Address = (dicFrmArray["address"]) as! String
                                    self.LocationAddress = Address
                                    print(self.LocationAddress)
                                    self.locationTextField.text = Address
                                    SVProgressHUD.dismiss()
                                    
                                    self.getAreasData(address: Address)
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                else{
                    print("Failure")
                    
                }
                break
            case .failure:
                if let json = response.result.value{
                    print(json)
                    
                }
                break
            }
        }
        
    }
    
    func getLocation() {
        
        locationTextField.text! = ""
        
        getAddress()
    }

}


extension CreatePostViewController {
    
    @IBAction func didTapPickup(_ sender: Any)
    {
        
        
//        locationTextField.text! = ""
        self.current_country = ""
        self.current_city = ""
        self.current_street = ""
        if(i<3)
        {
            SVProgressHUD.show(withStatus: "Detect Location")
            areaData.removeAll()
            getAddress()
            i = i + 1
            print(i)
        }
    }
}
extension String {
    
    static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
