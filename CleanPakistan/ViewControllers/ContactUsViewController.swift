//
//  ContactUsViewController.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 12/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ContactUsViewController: BaseViewController {

    @IBOutlet weak var feedbackTextField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.addNavBars("Contact Us")

        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        SVProgressHUD.dismiss()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func backAction(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButton(_ sender: UIButton) {
        let feedBackMsg = feedbackTextField.text!
        let Name = UserDefaults.standard.value(forKey: "Name") as! String
        let Email = UserDefaults.standard.value(forKey: "Email") as! String
        sendFeedback(message: feedbackTextField.text!, name: Name, email: Email)
    }
    
    
    //updateProfile
    func sendFeedback(message: String, name: String,email:String) {
        let url = AppConfig.sendFeedback
        
        
        
        let parameters = ["message": message,"name": name,"email" : email] as [String : Any]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success(let json):
                print(response)
                self.dismiss(animated: true, completion: nil)
                
                
            case .failure(let error):
                print("error")
            }
        }
    }
    
}
