//
//  NotificationsViewController.swift
//  CleanPakistan
//
//  Created by Apple on 17/12/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

var NotificationViewId = ""

class NotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let array = ["asad","usman"]
    
    var Address = [String]()
    var Status = [String]()
    var PhotosBefore = [String]()
    var Images = [UIImage]()
    var Complaint_Id = [String]()
    var Status_Id = [String]()
    var Customer_Remarks = [String]()
    var Employee_Remarks = [String]()

    var ComplaintMsg = [String]()
    var Time = [String]()

    @IBOutlet weak var NotificationTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        getAllNotifications()
        NotificationTable.delegate = self
        // Do any additional setup after loading the view.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Complaint_Id.count
    }
    var execute : Bool = false
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell", for: indexPath) as! NotificationsTableViewCell
   //     if(execute)
   //     {
        cell.notificationIdLbl?.text! = Complaint_Id[indexPath.row] + ":"
        cell.msgLbl?.text! = ComplaintMsg[indexPath.row]
        cell.msgLbl.numberOfLines = 0;
        cell.msgLbl.lineBreakMode = .byWordWrapping
        
        cell.addressLbl?.text! = Address[indexPath.row]
        cell.addressLbl.numberOfLines = 0;
        cell.addressLbl.lineBreakMode = .byWordWrapping
        
        print(Address[indexPath.row])
        
        cell.dateLbl?.text! = Time[indexPath.row]
        if(Status_Id[indexPath.row] == "1")
        {
            cell.remarksLbl?.text! = Customer_Remarks[indexPath.row]
            cell.remarksLbl.numberOfLines = 0;
            cell.remarksLbl.lineBreakMode = .byWordWrapping
        }
        else
        {
            cell.remarksLbl?.text! = Employee_Remarks[indexPath.row]
            cell.remarksLbl.numberOfLines = 0;
            cell.remarksLbl.lineBreakMode = .byWordWrapping
        }
        
            cell.statusLbl?.text! = Status[indexPath.row]
            
            let imageName = PhotosBefore[indexPath.row]
            
            let remoteImageURL = URL(string: AppConfig.downloadImageUrl+imageName)!
            
            // Use Alamofire to download the image
            Alamofire.request(remoteImageURL).responseData { response in
                guard let image = UIImage(data:response.data!) else {
                    // Handle error
                    print("No Image")
                    return
                }
                guard let imageData = image.jpegData(compressionQuality: 0.5) else{
                    return
                }
                cell.complaintImage.image = UIImage(data : imageData)
                cell.complaintImage.layer.masksToBounds = true
                cell.complaintImage.layer.cornerRadius = 4.0;
                // cell.complaintImage.contentMode = UIView.ContentMode.scaleAspectFit
                
            }
    
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Garbage_id = Complaint_Id[indexPath.row]
        beforeViewSingle = "1"
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ViewComplaintsViewController")
    //    self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
        
        
    }
    
    
    func getAllNotifications(){
        AppConfig.stopAllSessions()

        SVProgressHUD.show(withStatus: "Please wait")
        
        var url = String()
        var parameters = [String: String]()
        
        url = AppConfig.showAllNotifications

        let email = UserDefaults.standard.value(forKey: "Email") as! String
        print(email)
        let rol = "0"
        parameters = ["user_email": email,"roll": rol]
        
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result){
            case .success:
                if let json = response.result.value{
                  //  self.clearTable()
                    let JSON = json as! NSDictionary
                    print(JSON.allKeys)
                    print(JSON)
                    for res in JSON{
                        if ("\(res.key)" == "records")
                        {
                            record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                            for innerResult in record{
                                // print(innerResult)
                                
                                let dicFrmArray = innerResult
                                
                                if ((dicFrmArray["address"] as? NSNull) == nil && dicFrmArray["emp_remarks"] != nil && dicFrmArray["customer_remark"] != nil && dicFrmArray["id"] != nil && dicFrmArray["msg"] != nil && dicFrmArray["photo"] != nil && dicFrmArray["photo"] != nil && dicFrmArray["status_id"] != nil && dicFrmArray["time"] != nil && dicFrmArray["status_name"] != nil ){
                                    
                                    
                                    let Address = (dicFrmArray["address"] )
                                    let Status_Name = (dicFrmArray["status_name"] )
                                    let photos = (dicFrmArray["photo"] )
                                    let ComplaintId = (dicFrmArray["id"])
                                    let StatusId = (dicFrmArray["status_id"])
                                    let CompMsg = (dicFrmArray["msg"])
                                    let EmployeeRemarks = (dicFrmArray["emp_remarks"])
                                    let CustomerRemarks = (dicFrmArray["customer_remark"])
                                    let NotificationTime = (dicFrmArray["time"])
                                
                                
                                    print(ComplaintId as! String)
                                
                                    self.Address.append(Address as! String)
                                    self.Status.append(Status_Name as! String)
                                    self.PhotosBefore.append(photos as! String)
                                    self.Complaint_Id.append(ComplaintId as! String)
                                    self.Status_Id.append(StatusId as! String)
                                    self.Employee_Remarks.append(EmployeeRemarks as! String)
                                    self.Customer_Remarks.append(CustomerRemarks as! String)
                                    self.ComplaintMsg.append(CompMsg as! String)
                                    self.Time.append(NotificationTime as! String)
                              
                                    
                                    
                                }
                                
                            }
                           // self.execute = true
                            SVProgressHUD.dismiss()
                            self.NotificationTable.reloadData()
                                for data in self.Complaint_Id
                                {
                                    print(data)
                                }
                            
                        }
                    }
                    
                }
                else{
                    print("Failure")
                    SVProgressHUD.dismiss()
                }
                break
            case .failure:
                if let json = response.result.value{
                    print(json)
                    SVProgressHUD.dismiss()
                }
                break
            }
        }
        
    }

}
