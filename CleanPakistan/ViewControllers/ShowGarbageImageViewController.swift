//
//  ShowGarbageImageViewController.swift
//  CleanPakistan
//
//  Created by Apple on 14/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ShowGarbageImageViewController: UIViewController {

   // @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    var imageName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if imageShow == "BeforeImage"
        {
            imageName = beforeImageName
            getImage(name: imageName)
        }
        else if imageShow == "AfterImage"
        {
            imageName = afterImageName
            getImage(name: imageName)
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        SVProgressHUD.dismiss()
    }
    
    func getImage(name: String){
        
        SVProgressHUD.show(withStatus: "Please Wait")
        let imageName = name
        // print(PhotoBefore)
        let remoteImageURLBefore = URL(string: AppConfig.downloadImageUrl+imageName)!
        
        // Use Alamofire to download the image
        Alamofire.request(remoteImageURLBefore).responseData { response in
            guard let image = UIImage(data:response.data!) else {
                // Handle error
                print("No Image")
                return
            }
            let imageData = image.jpegData(compressionQuality: 0.3)
            self.imgView.image = UIImage(data : imageData!)
            SVProgressHUD.dismiss()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
