//
//  UcHomeViewController.swift
//  CleanPakistan
//
//  Created by Apple on 06/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
var UcComplaintCategory : String = ""
class UcHomeViewController: BaseViewController {

    
    @IBOutlet weak var UCcomplainView:UIView!
    @IBOutlet weak var UCcompletedComplainView:UIView!
    @IBOutlet weak var UCpendingComplainView:UIView!
    @IBOutlet weak var UCprocessingComplainView:UIView!
    
    @IBOutlet weak var AllComplaintsLabel: UILabel!
    @IBOutlet weak var UCcomplainLabel:UILabel!
    
    @IBOutlet weak var UCcompletedComplainLabel:UILabel!
    @IBOutlet weak var UCpendingComplainLabel:UILabel!
    @IBOutlet weak var UCprocessingComplainLabel:UILabel!
    
    
    var allComplaintsCount = String()
    var pendingCount = String()
    var closedCount = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AllComplaintsLabel.text = "ALL COMPLAINTS"
        AllComplaintsLabel.numberOfLines = 0
        getAllComplaintsCount()
        getAllPendingCount()
        getAllClosedCount()
        getAllProcessingCount()
        prepareUI()
        self.addNavBars("Home")
        self.UCaddSideMenu()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func allComplaintsButton(_ sender: UIButton) {
        UcComplaintCategory = "AllComplaints"
        let a = self.storyboard?.instantiateViewController(withIdentifier: "AllComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
    }
    
    @IBAction func allProcessingButton(_ sender: UIButton) {
        UcComplaintCategory = "AllProcessing"
        let a = self.storyboard?.instantiateViewController(withIdentifier: "UcAllComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
    }
    
    @IBAction func allPendingButton(_ sender: Any) {
        UcComplaintCategory = "AllPending"
        let a = self.storyboard?.instantiateViewController(withIdentifier: "UcAllComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
    }
    
    @IBAction func allClosedButton(_ sender: Any) {
        UcComplaintCategory = "AllClosed"
        let a = self.storyboard?.instantiateViewController(withIdentifier: "UcAllComplaintsViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
    }
    
    // get getAllComplaintsCount
    func getAllComplaintsCount(){
        let url = AppConfig.getAllUCCountComplaints
        print(url)
        let uc_area_id = 11//
        let parameters = ["uc_area_id": uc_area_id]
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch response.result{
            case .success:
                if let json = response.result.value{
                    let JSON = json as! NSDictionary
                    print(JSON.allKeys)
                    print(JSON)
                    for res in JSON{
                        if ("\(res.key)" == "records")
                        {
                            record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                            for innerResult in record{
                                // print(innerResult)
                                
                                let dicFrmArray = innerResult
                                
                                if ((dicFrmArray["all_complaints"] as? NSNull) == nil ){
                                    let count = (dicFrmArray["all_complaints"])
                                    
                                    
                                    self.allComplaintsCount = (count as! String)
                                    
                                    self.UCcomplainLabel.text = self.allComplaintsCount
                                    
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                else{
                    print("Failure")
                }
                break
            case .failure:
                if let json = response.result.value{
                    print(json)
                }
                break
            }
        }
        
    }
    
    // getAllPendingCount
    func getAllPendingCount(){
        let url = AppConfig.getAllCountPendingUCComplaints
        print(url)
        let uc_area_id = 11
        let parameters = ["uc_area_id": uc_area_id]
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch response.result{
            case .success:
                if let json = response.result.value{
                    let JSON = json as! NSDictionary
                    print(JSON.allKeys)
                    print(JSON)
                    for res in JSON{
                        if ("\(res.key)" == "records")
                        {
                            record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                            for innerResult in record{
                                // print(innerResult)
                                
                                let dicFrmArray = innerResult
                                
                                if ((dicFrmArray["all_uc_pending_compliants"] as? NSNull) == nil ){
                                    let count = (dicFrmArray["all_uc_pending_compliants"])
                                    
                                    self.pendingCount = (count as! String)
                                    
                                    self.UCpendingComplainLabel.text = self.pendingCount
                                    
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                else{
                    print("Failure")
                }
                break
            case .failure:
                if let json = response.result.value{
                    print(json)
                }
                break
            }
        }
        
    }
    
    // getAllClosedCount
    func getAllClosedCount(){
        let url = AppConfig.getCountAllFinishUCComplaint
        print(url)
        let uc_area_id = 11
        let parameters = ["uc_area_id": uc_area_id]
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch response.result{
            case .success:
                if let json = response.result.value{
                    let JSON = json as! NSDictionary
                    print(JSON.allKeys)
                    print(JSON)
                    for res in JSON{
                        if ("\(res.key)" == "records")
                        {
                            record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                            for innerResult in record{
                                // print(innerResult)
                                
                                let dicFrmArray = innerResult
                                
                                if ((dicFrmArray["all_uc_finish_compliants"] as? NSNull) == nil ){
                                    let count = (dicFrmArray["all_uc_finish_compliants"])
                                    
                                    self.closedCount = (count as! String)
                                    
                                    self.UCcompletedComplainLabel.text = self.closedCount
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                else{
                    print("Failure")
                }
                break
            case .failure:
                if let json = response.result.value{
                    print(json)
                }
                break
            }
        }
        
    }

    // getAllClosedCount
    func getAllProcessingCount(){
        let url = AppConfig.getCountAllProcessUCComplaint
        print(url)
        let uc_area_id = 11
        let parameters = ["uc_area_id": uc_area_id]
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch response.result{
            case .success:
                if let json = response.result.value{
                    let JSON = json as! NSDictionary
                    print(JSON.allKeys)
                    print(JSON)
                    for res in JSON{
                        if ("\(res.key)" == "records")
                        {
                            record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                            for innerResult in record{
                                // print(innerResult)
                                
                                let dicFrmArray = innerResult
                                
                                if ((dicFrmArray["all_process_complaints"] as? NSNull) == nil ){
                                    let count = (dicFrmArray["all_process_complaints"])
                                    
                                    self.closedCount = (count as! String)
                                    
                                    self.UCprocessingComplainLabel.text = self.closedCount
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                else{
                    print("Failure")
                }
                break
            case .failure:
                if let json = response.result.value{
                    print(json)
                }
                break
            }
        }
        
    }
}
extension UcHomeViewController {
    
    func prepareUI(){
        
        UCcomplainView.layer.cornerRadius = 5
        UCcomplainView.clipsToBounds = true
        
        UCcomplainView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: 2.0, opacity: 0.35)
        
        UCcompletedComplainView.layer.cornerRadius = 5
        UCcompletedComplainView.clipsToBounds = true
        
        UCcompletedComplainView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: 2.0, opacity: 0.35)
        
        UCprocessingComplainView.layer.cornerRadius = 5
        UCprocessingComplainView.clipsToBounds = true
        
        UCprocessingComplainView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: 2.0, opacity: 0.35)
        
        UCpendingComplainView.layer.cornerRadius = 5
        UCpendingComplainView.clipsToBounds = true
        
        UCpendingComplainView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: 2.0, opacity: 0.35)
    }
}
