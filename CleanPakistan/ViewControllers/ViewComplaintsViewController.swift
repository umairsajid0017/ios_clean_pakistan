//
//  ViewComplaintsViewController.swift
//  CleanPakistan
//
//  Created by Apple on 12/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

var beforeViewSingle : String = ""


var beforeImageName: String = ""
var afterImageName: String = ""
var imageShow: String = ""

class ViewComplaintsViewController: UIViewController {

    @IBOutlet weak var postedDateLbl: UILabel!
    @IBOutlet weak var processDateLbl: UILabel!
    @IBOutlet weak var closeDateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var areaNameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var employeeNameLbl: UILabel!
    @IBOutlet weak var userRemarksLbl: UILabel!
    @IBOutlet weak var employeeRemarksLbl: UILabel!
    @IBOutlet weak var complaintTypeLbl: UILabel!
    @IBOutlet weak var beforeUserNameLbl: UILabel!
    
    @IBOutlet weak var beforeImgView: UIImageView!
    @IBOutlet weak var afterImgView: UIImageView!
    @IBOutlet weak var beforeviewImgView: UIImageView!
    
    
    
    
    
    @IBOutlet weak var beforeUserRemarksLbl: UILabel!
    @IBOutlet weak var beforeNameView: RoundedShadowView!
    @IBOutlet weak var afterNameView: RoundedShadowView!
    
    @IBOutlet weak var beforeImageView: RoundedShadowView!
    @IBOutlet weak var afterImageView: RoundedShadowView!
    
    @IBOutlet weak var beforeRemarksView: RoundedShadowView!
    @IBOutlet weak var afterRemarksView: RoundedShadowView!
    @IBOutlet weak var beforeLabel: UILabel!
    @IBOutlet weak var afterLabel: UILabel!
    @IBOutlet weak var userRemarksLabel: UILabel!
    @IBOutlet weak var employeeRemarksLabel: UILabel!
    
    @IBOutlet weak var BeforeView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
/*
        if(beforeViewSingle == "1")
        {
            BeforeView.isHidden = false
            
            beforeLabel.isHidden = true
            afterLabel.isHidden = true
            afterNameView.isHidden = true
            beforeNameView.isHidden = true
            beforeImageView.isHidden = true
            afterImageView.isHidden = true
            userRemarksLabel.isHidden = true
            employeeRemarksLabel.isHidden = true
            beforeRemarksView.isHidden = true
            afterRemarksView.isHidden = true
           // beforeViewSingle = ""
        }
        else
        {
            BeforeView.isHidden = true
        }
        */
       // BeforeView.isHidden = true
        
        postedDateLbl.numberOfLines = 0;
        postedDateLbl.lineBreakMode = .byWordWrapping
        
        processDateLbl.numberOfLines = 0;
        processDateLbl.lineBreakMode = .byWordWrapping
        
        closeDateLbl.numberOfLines = 0;
        closeDateLbl.lineBreakMode = .byWordWrapping
        
        userRemarksLbl.numberOfLines = 0;
        userRemarksLbl.lineBreakMode = .byWordWrapping
        
        beforeUserRemarksLbl.numberOfLines = 0;
        beforeUserRemarksLbl.lineBreakMode = .byWordWrapping
        
        employeeRemarksLbl.numberOfLines = 0;
        employeeRemarksLbl.lineBreakMode = .byWordWrapping
        AppConfig.stopAllSessions()
        getViewComplaints(Id: Garbage_id)
        
        let gestureRecognizerBefore: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(beforeImageViewTapped))
        beforeImgView.addGestureRecognizer(gestureRecognizerBefore)
        beforeImgView.isUserInteractionEnabled = true
        
        let gestureRecognizerAfter: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(afterImageViewTapped))
        afterImgView.addGestureRecognizer(gestureRecognizerAfter)
        afterImgView.isUserInteractionEnabled = true
        
        let gestureRecognizerBeforeSingle: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(beforeSingleImageViewTapped))
        beforeviewImgView.addGestureRecognizer(gestureRecognizerBeforeSingle)
        beforeviewImgView.isUserInteractionEnabled = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
      //  SVProgressHUD.show(withStatus: "Please wait")
    }
    override func viewDidDisappear(_ animated: Bool) {
        beforeViewSingle = ""
        SVProgressHUD.dismiss()
    }
    @objc func beforeImageViewTapped() {
        imageShow = "BeforeImage"
        print("BeforeImage")
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ShowGarbageImageViewController")
        self.navigationController?.pushViewController(a!, animated: true)
       // self.present(a!, animated: true, completion: nil)
    }
    
    @objc func beforeSingleImageViewTapped() {
        imageShow = "BeforeImage"
        print("BeforeImage")
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ShowGarbageImageViewController")
        self.navigationController?.pushViewController(a!, animated: true)
        // self.present(a!, animated: true, completion: nil)
    }
    
    @objc func afterImageViewTapped() {
        imageShow = "AfterImage"
        print("AfterImage")
        let a = self.storyboard?.instantiateViewController(withIdentifier: "ShowGarbageImageViewController")
        self.navigationController?.pushViewController(a!, animated: true)
        // self.present(a!, animated: true, completion: nil)
    }
    @IBAction func backButton(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }

   
    
    
    func getViewComplaints(Id: String) -> Void {
        
        
        SVProgressHUD.show(withStatus: "Please Wait")
        let ID = Garbage_id
        print(ID)
        let parameters = ["id": ID]
        let url = AppConfig.ViewComplaints
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            responce in
            switch responce.result{
            case .success:
                if let json = responce.result.value{
                    let JSON = json as! NSDictionary
                    print(JSON.allKeys)
                    print(JSON)
                    for res in JSON{
                        if ("\(res.key)" == "records")
                        {
                            record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                            for innerResult in record{
                                // print(innerResult)
                                
                                let data = innerResult
                                
                                if ((data["id"] as? NSNull) == nil && data["user_email"] != nil && data["garbage_type"] != nil && data["photo"] != nil ){
                                   // let Id = (data["id"])
                                  //  let Garbage_Name = (data["garbage_name"] )
                                    
//                                    let Id = data["id"] as! String
//                                    let Useremail = data["user_email"] as! String
//                                    let GarbageType = data["garbage_type"] as! String
                                    
                                    let CustomerRemarks = data["customer_remark"] as! String
//                                    let Address = data["address"] as! String
//                                    let Location = data["location"] as! String
//                                    let CityId = data["city_id"] as! String
//                                    let StatusId = data["status_id"] as! String
//                                    let UcAreaId = data["uc_area_id"] as! String
                                    var PostedAt = data["posted_at"] as! String
                                    if(PostedAt.contains("PM"))
                                    {
                                        let date = PostedAt.components(separatedBy: "PM")//split(separator: "PM")
                                        print(date[0])
                                        print(date[1])
                                        PostedAt = ("\(date[0])\n\(date[1])")
                                        print(PostedAt)
                                    }
                                    else if(PostedAt.contains("AM"))
                                    {
                                        let date = PostedAt.components(separatedBy: "AM")//split(separator: "PM")
                                        print(date[0])
                                        print(date[1])
                                        PostedAt = ("\(date[0])\n\(date[1])")
                                        print(PostedAt)
                                    }
                                    
                                    
                                    var ProcessedDate = data["processed_date"] as! String
                                    if(ProcessedDate.contains("PM"))
                                    {
                                        let date = ProcessedDate.components(separatedBy: "PM")//split(separator: "PM")
                                        print(date[0])
                                        print(date[1])
                                        ProcessedDate = ("\(date[0])\n\(date[1])")
                                        print(ProcessedDate)
                                    }
                                    else if(ProcessedDate.contains("AM"))
                                    {
                                        let date = ProcessedDate.components(separatedBy: "AM")//split(separator: "PM")
                                        print(date[0])
                                        print(date[1])
                                        ProcessedDate = ("\(date[0])\n\(date[1])")
                                        print(ProcessedDate)
                                    }
                                    
                                    var ClosedDate: String = "00-00-00"
                                    if (data["id"] as? NSNull) != nil
                                    {
                                        ClosedDate = "00-00-00"
                                    }
                                    else
                                    {
                                        ClosedDate = data["closed_date"] as! String
                                        if(ClosedDate.contains("PM"))
                                        {
                                            let date = ClosedDate.components(separatedBy: "PM")//split(separator: "PM")
                                            print(date[0])
                                            print(date[1])
                                            ClosedDate = ("\(date[0])\n\(date[1])")
                                            print(ClosedDate)
                                        }
                                        else if(ClosedDate.contains("AM"))
                                        {
                                            let date = ClosedDate.components(separatedBy: "AM")//split(separator: "PM")
                                            print(date[0])
                                            print(date[1])
                                            ClosedDate = ("\(date[0])\n\(date[1])")
                                            print(ClosedDate)
                                        }
                                    }
                                    
                                    var EmployeeRemarks: String = ""
                                    if (data["emp_remarks"] as? NSNull) != nil
                                    {
                                        EmployeeRemarks = ""
                                    }
                                    else
                                    {
                                        EmployeeRemarks = data["emp_remarks"] as! String
                                    }
                                    // some thing wrong 3/12/19
//                                    let EmployeeEmail = data["emp_email"] as! String
                                  //  let EmployeeRemarks = data["emp_remarks"] as! String
                                    
                                    let AreaName = data["area_name"] as! String
                                    let GarbageName = data["garbage_name"] as! String
                                    let StatusName = data["status_name"] as! String
                                    let UserName = data["user_name"] as! String
//                                    let UserPhoto = data["user_phone"] as! String
//                                   let UserImg = data["user_img"] as! String
                                  //  let EmployeeName = data["emp_name"] as! String
                                    
                                    var EmployeeName: String = ""
                                    if (data["emp_name"] as? NSNull) != nil
                                    {
                                        EmployeeName = ""
                                    }
                                    else
                                    {
                                        EmployeeName = data["emp_name"] as! String
                                    }
//                                    let EmployeePhone = data["emp_phone"] as! String
//                                    let EmployeeImg = data["emp_img"] as! String
                                    
                                    self.userRemarksLbl.text = CustomerRemarks
                                    self.beforeUserRemarksLbl.text = CustomerRemarks
                                    self.employeeRemarksLbl.text = EmployeeRemarks
                                    
                                    if (StatusName == "Pending")
                                    {
                                        self.processDateLbl.text = "00-00-00"
                                    }
                                    else
                                    {
                                        self.processDateLbl.text = ProcessedDate
                                    }
                                    
                                    if (StatusName == "Pending")
                                    {
                                        self.closeDateLbl.text = "00-00-00"
                                    }
                                    else
                                    {
                                        self.closeDateLbl.text = ClosedDate
                                    }
                                    
                                    self.postedDateLbl.text = PostedAt
                                    
                                    
                                    self.areaNameLbl.text = AreaName
                                    self.userNameLbl.text = UserName
                                    self.beforeUserNameLbl.text = UserName
                                    self.employeeNameLbl.text = EmployeeName
                                    self.statusLbl.text = StatusName
                                    
                                    if(StatusName == "Pending")
                                    {
                                        self.BeforeView.isHidden = false
                                        
                                        self.beforeLabel.isHidden = true
                                        self.afterLabel.isHidden = true
                                        self.afterNameView.isHidden = true
                                        self.beforeNameView.isHidden = true
                                        self.beforeImageView.isHidden = true
                                        self.afterImageView.isHidden = true
                                        self.userRemarksLabel.isHidden = true
                                        self.employeeRemarksLabel.isHidden = true
                                        self.beforeRemarksView.isHidden = true
                                        self.afterRemarksView.isHidden = true
                                        // beforeViewSingle = ""
                                    }
                                    else
                                    {
                                        self.BeforeView.isHidden = true
                                    }
                                    
                                   // self.complaintTypeLbl.text = GarbageName
                                    self.navigationItem.title = GarbageName
                                    
                                 //   let PhotoBefore = data["photo"] as! String
                                    
                                    var PhotoBefore: String = "placeholder.png"
                                    if (data["photo"] as? NSNull) != nil
                                    {
                                        PhotoBefore = "placeholder.png"
                                    }
                                    else
                                    {
                                        PhotoBefore = data["photo"] as! String
                                    }
                                    
                                    
                                    
                                    beforeImageName = PhotoBefore
                                    print(PhotoBefore)
                                    let remoteImageURLBefore = URL(string: AppConfig.downloadImageUrl+PhotoBefore)!
                                    
                                    // Use Alamofire to download the image
                                    Alamofire.request(remoteImageURLBefore).responseData { response in
                                        guard let image = UIImage(data:response.data!) else {
                                            // Handle error
                                            print("No Image")
                                            SVProgressHUD.dismiss()
                                            return
                                        }
                                        let imageData = image.jpegData(compressionQuality: 0.5)
                                        if(beforeViewSingle == "1")
                                        {
                                            self.beforeImgView.image = UIImage(data : imageData!)
                                        }
                                        else
                                        {
                                            //beforeImgView
                             //               self.beforeImgView.image = UIImage(data : imageData!)
                                            
                                            self.beforeviewImgView.image = UIImage(data : imageData!)
                                        }
                                        
                                        
                                        SVProgressHUD.dismiss()
                                    }
                                    
                                //    let PhotoAfter = data["photo_after"] as! String
                                    
                                    var PhotoAfter: String = "placeholder.png"
                                    if (data["photo_after"] as? NSNull) != nil
                                    {
                                        PhotoAfter = "placeholder.png"
                                    }
                                    else
                                    {
                                        PhotoAfter = data["photo_after"] as! String
                                    }
                                    afterImageName = PhotoAfter
                                    print(PhotoAfter)
                                    let remoteImageURLAfter = URL(string: AppConfig.downloadImageUrl+PhotoAfter)!
                                    
                                    // Use Alamofire to download the image
                                    Alamofire.request(remoteImageURLAfter).responseData { response in
                                        guard let image = UIImage(data:response.data!) else {
                                            // Handle error
                                            print("No Image")
                                            SVProgressHUD.dismiss()
                                            return
                                        }
                                        let imageData = image.jpegData(compressionQuality: 0.5)
                                        self.afterImgView.image = UIImage(data : imageData!)
                                        SVProgressHUD.dismiss()
                                    }

                                    //SVProgressHUD.dismiss()
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                else{
                    print("Failure")
                    SVProgressHUD.dismiss()
                }
                break
            case .failure:
                if let json = responce.result.value{
                    print(json)
                    SVProgressHUD.dismiss()
                }
                break
            }
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



