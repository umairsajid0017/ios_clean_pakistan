//
//  ShowFeedbackViewController.swift
//  CleanPakistan
//
//  Created by Apple on 30/12/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos
import SVProgressHUD

class ShowFeedbackViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    var feedback_data = [Feedback]()
    var complaintIdArray = [String]()
    var dateArray = [String]()
    var comentArray = [String]()
    var respondArray = [String]()
    var satisfiedArray = [String]()
    var ratingArray = [String]()
    //var myArray = [Double]()
    
    @IBOutlet weak var feedbackTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        getFeedbackData()
        
        // Do any additional setup after loading the view.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return complaintIdArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowFeedbackTableViewCell", for: indexPath) as! ShowFeedbackTableViewCell
        cell.ratingView.settings.updateOnTouch = false
        cell.ratingView.settings.fillMode = .precise
        cell.ratingView.settings.emptyColor = .lightGray
        cell.ratingView.settings.emptyBorderColor = .lightGray
        cell.ratingView.settings.filledBorderColor = .lightGray
        cell.ratingView.settings.starSize = 15.0
        
        cell.complaintIdLabel.text! = "Complaint ID: "+complaintIdArray[indexPath.row]
        cell.dateLabel.text! = dateArray[indexPath.row]
        cell.comentLabel.text! = comentArray[indexPath.row]
        
        //Satisfied
        if(satisfiedArray[indexPath.row] == "1")
        {
            cell.satisfyLabel.text! = "Satisfied"
        }
        else if(satisfiedArray[indexPath.row] == "0")
        {
            cell.satisfyLabel.text! = "Not Satisfied"
        }
        
        //Respond
        if(respondArray[indexPath.row] == "1")
        {
            cell.respondLabel.text! = "Responded"
        }
        else if(respondArray[indexPath.row] == "0")
        {
            cell.respondLabel.text! = "Not Responded"
        }
        
        cell.ratingView.rating = Double(ratingArray[indexPath.row])!
        
        return cell
        
    }
    
    func getFeedbackData(){
        
        AppConfig.stopAllSessions()
        
        SVProgressHUD.show(withStatus: "Please Wait")
        
        var status: String = ""
        
        if ComplaintCategory == "Positive"
        {
            status = "1"
        }
        else if ComplaintCategory == "Negative"
        {
            status = "-1"
        }
        
        
        let url = AppConfig.showfeedback
        print(url)
        let userEmail = UserDefaults.standard.value(forKey: "Email") as! String
        print(userEmail)
        
        
        let parameters = ["user_email": userEmail, "status": status]
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result){
            case .success:
                if let json = response.result.value{
                    let JSON = json as! NSDictionary
                    print(JSON.allKeys)
                    print(JSON)
                    for res in JSON{
                        if ("\(res.key)" == "records")
                        {
                            record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                            for innerResult in record{
                                // print(innerResult)
                                
                                let dicFrmArray = innerResult
                                    
                                    if ((dicFrmArray["id"] as? NSNull) == nil && dicFrmArray["responded"] != nil && dicFrmArray["rating"] != nil && dicFrmArray["current_date"] != nil && dicFrmArray["complaint_id"] != nil && dicFrmArray["comment"] != nil && dicFrmArray["satisfy"] != nil && dicFrmArray["resolution_id"] != nil ){
                                        
                                        let Id = (dicFrmArray["id"])
                                        let Responded = (dicFrmArray["responded"] )
                                        let Rating = (dicFrmArray["rating"] )
                                        let CurrentDate = (dicFrmArray["current_date"] )
                                        let Complaint_ID = (dicFrmArray["complaint_id"] )
                                        let Coment = (dicFrmArray["comment"] )
                                        let Satisfied = (dicFrmArray["satisfy"])
                                        
                                        
                                        
                                        self.complaintIdArray.append(Complaint_ID as! String)
                                        self.respondArray.append(Responded as! String)
                                        self.ratingArray.append(Rating as! String)
                                        self.dateArray.append(CurrentDate as! String)
                                        self.comentArray.append(Coment as! String)
                                        self.satisfiedArray.append(Satisfied as! String)
                                        
                                        /*
                                        let data = Feedback(responded: Responded as! String, rating: Rating as! String, current_date: CurrentDate as! String, complaint_id: ComplaintId as! String, comment: Coment as! String, satisfy: Satisfied as! String)
                                        
                                        self.feedback_data.append(data)
                                        */
                                    }
                                    
                                }
                                
                            }
                        }
                        SVProgressHUD.dismiss()
                        self.feedbackTableView.reloadData()
                    }
                    else{
                        print("Failure")
                    }
                    break
                case .failure:
                    if let json = response.result.value{
                        print(json)
                    }
                    break
                }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
