//
//  EditProfileViewController.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 12/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import MobileCoreServices
import AVKit
import SwiftyJSON
import SVProgressHUD


class EditProfileViewController: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNoTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var imgView: RoundedImageView!
    @IBOutlet weak var locationLbl: UILabel!
    
    let image_Picker = UIImagePickerController()
    var imageName : String = "placeholder_user.png"
   // UserDefaults.standard.set(image, forKey:"Image")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.addNavBars("Edit Profile")
        image_Picker.delegate = self

        nameTextField.text = UserDefaults.standard.value(forKey: "Name") as? String
        emailTextField.text = UserDefaults.standard.value(forKey: "Email") as? String
        phoneNoTextField.text = UserDefaults.standard.value(forKey: "Phone") as? String
        
        addressTextField.text = UserDefaults.standard.value(forKey: "Address") as? String
        
        if user_role == "1"
        {
            locationLbl.text = "Area"
            cityTextField.text = UserDefaults.standard.value(forKey: "Area_Name") as? String
            
        }
        else if user_role == "0"
        {
            locationLbl.text = "City"
            cityTextField.text = UserDefaults.standard.value(forKey: "City_Name") as? String
            
        }
        
        let PhotoName = UserDefaults.standard.value(forKey: "Image") as! String
        if PhotoName == ""
        {
            getImage(name: imageName)
        }
        else
        {
            getImage(name: PhotoName)
        }
       // getImage(name: PhotoName)
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        SVProgressHUD.dismiss()
    }
    @IBAction func profileImageEditButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Media Profile", message: "Please Select A Photo", preferredStyle: .actionSheet);
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil);
        let photos = UIAlertAction(title: "Photos", style: .default, handler: {(alert : UIAlertAction) in
            self.chooseMedia(type: kUTTypeImage)
        })
        
        alert.addAction(photos);
        alert.addAction(cancel);
        present(alert, animated: true, completion: nil)
    }
    // Browse Profile Image Functions
    private func chooseMedia(type: CFString){
        image_Picker.mediaTypes = [type as String]
        present(image_Picker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageName = String.random() //UUID().uuidString
        imageName = imageName+".png"
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            // selectedImage = editedImage
            imgView.image = image
            picker.dismiss(animated: true, completion: nil)
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // selectedImage = originalImage
            imgView.image = image
            //imgView.needsDisplay = true
            picker.dismiss(animated: true, completion: nil)
        } else {
            print("Something went wrong")
        }
    }// end Browse Profile Image Functions

    
    // Back Button
    @IBAction func backAction(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }

    // Save Button
    @IBAction func saveButton(_ sender: Any) {
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            UserDefaults.standard.set(imageName, forKey: "Image")
            updateProfile(image: imageName, name: nameTextField.text!, phone: phoneNoTextField.text!, address: addressTextField.text!, email: emailTextField.text!)
            
            uploadImage(imgView.image)
        }
        else{
            let msg = "No Internet Connection"
            AppConfig.toastMessage(msg)
        }
        
    }
    
    // get Image
    func getImage(name: String){
        
        SVProgressHUD.show(withStatus: "Please Wait")
         let imageName = name
        // print(PhotoBefore)
        let remoteImageURLBefore = URL(string: AppConfig.downloadImageUrl+imageName)!
        
        // Use Alamofire to download the image
        Alamofire.request(remoteImageURLBefore).responseData { response in
            guard let image = UIImage(data:response.data!) else {
                // Handle error
                print("No Image")
                return
            }
            let imageData = image.jpegData(compressionQuality: 0.5)
            self.imgView.image = UIImage(data : imageData!)
            SVProgressHUD.dismiss()
        }
    }
    

    
    //updateProfile
    func updateProfile(image: String, name: String,phone:String, address: String, email: String) {
        let url = AppConfig.profileUpdate
        
        print(imageName+" complainPost")
        
        let parameters = ["image": image,"name": name,"phone" : phone,"address" : address,"email" : email] as [String : Any]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success(let json):
                print(response)
                
                UserDefaults.standard.set(nil, forKey: "Image")
                UserDefaults.standard.set(nil, forKey: "Name")
                UserDefaults.standard.set(nil, forKey: "Phone")
                UserDefaults.standard.set(nil, forKey: "Address")
                UserDefaults.standard.set(nil, forKey: "Email")
                
                UserDefaults.standard.set(image, forKey: "Image")
                UserDefaults.standard.set(name, forKey: "Name")
                UserDefaults.standard.set(phone, forKey: "Phone")
                UserDefaults.standard.set(address, forKey: "Address")
                UserDefaults.standard.set(email, forKey: "Email")
                
                self.dismiss(animated: true, completion: nil)
                //print(UserDefaults.standard.value(forKey: "Phone"))
                
                break
                
            case .failure(let error):
                print("error")
            }
        }
    }
    
    //Image Upload
    func uploadImage(_ profileImage:UIImage?){
        
        SVProgressHUD.show(withStatus: "Updating Profile")
        
        let url = AppConfig.imageUploadUrl
        
        guard let profileImage = profileImage else {
            return
        }
        
        guard let imageDATA = profileImage.jpegData(compressionQuality: 0.45) else { return }
        
        //let email = UserDefaults.standard.value(forKey: "Email")
        // let imageName = UUID().uuidString
        //  print(imageName)
        
        let imageData = imageDATA
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "image",fileName: "\(self.imageName)", mimeType: "image/png")
        },to:url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    
                    let with100  =  progress.fractionCompleted * 100
                    
                    let xx = Int(with100)
                    
                    print("Upload Progress With Int: \(xx)")
                    
                })
                
                upload.responseString{ response in
                 //   print(response.result.value)
                    
                    SVProgressHUD.dismiss()
                    print("Done With Uploading")
                    
                    //self..isEnabled = true
                    //   self.imageUploaded = true
                }
                
            case .failure(let encodingError):
                
                //  self.profileImage.image
                print(encodingError)
            }
        }
    }
   
    
}

