//
//  SignUpViewController.swift
//  CleanPakistan
//
//  Created by Apple on 27/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MobileCoreServices
import AVKit
import SVProgressHUD

struct CitiesStruct {
    let id: Int
    let name : String
}



class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {


    var imageName : String = "placeholder_user.png"
    
   // var city = ["Liaqatabad","Faisalabad","Islamabad"]
    var citiesArray = [String]()
    var citiesIdArray = [String]()
    var cityDict : [Int:String] = [:]
   // var dict : [Int: [Subject]] = [:]
    var base64String = String()
    let imagePicker = UIImagePickerController()
    
    
    let Tray = Data()
    
    @IBOutlet weak var signUpButton: ShadowButton!
    @IBOutlet weak var imgView: RoundedImageView!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirm_passwordTxt: UITextField!
    @IBOutlet weak var phoneNumTxt: UITextField!
    @IBOutlet weak var addressTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var locationTxt: UITextField!
    @IBOutlet weak var cityTable: UITableView!
    var dictionary: [String: String] = [:]
    var picUploadStatus = "false"
    
    var imageSelected = "notSelected"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        cityTxt.delegate = self
        
       // imgView.makeRounded()
        getCityData()
        cityTable.isHidden = true
        imagePicker.delegate = self
       // signUpButton.isEnabled = false
        
        // Do any additional setup after loading the view.
    }

    @IBAction func shiftTologinBtn(_ sender: UIButton) {
        let a = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
        // self.navigationController?.pushViewController(a!, animated: true)
        self.present(a!, animated: true, completion: nil)
    }
    

 //   @IBAction func cityBtn(_ sender: UIButton) {
  //      cityTable.isHidden = false
  //  }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // Show your picker here, and return false to avoid showing keyboard
        cityTable.isHidden = false
        return false
    }
    // Image Browse Button
    @IBAction func BrowseImageBtn(_ sender: RoundedButton) {
        
        
        let alert = UIAlertController(title: "Media Profile", message: "Please Select A Photo", preferredStyle: .actionSheet);
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil);
        let photos = UIAlertAction(title: "Photos", style: .default, handler: {(alert : UIAlertAction) in
            self.chooseMedia(type: kUTTypeImage)
        })
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(alert : UIAlertAction) in
            self.openCamera()
        })
        alert.addAction(photos);
        alert.addAction(camera)
        alert.addAction(cancel);
        present(alert, animated: true, completion: nil)
    }

    // Browse Profile Image Functions
    private func chooseMedia(type: CFString){
        imagePicker.mediaTypes = [type as String]
        present(imagePicker, animated: true, completion: nil)
    }
    private func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imageSelected = "selected"
            signUpButton.isEnabled = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            imageName = String.random() //UUID().uuidString
            imageName = imageName+".png"
            
            selectedImage = editedImage
            imgView.image = selectedImage!
            imageSelected = "selected"
            signUpButton.isEnabled = false
            uploadImage(imgView.image)
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            imageName = UUID().uuidString
            imageName = imageName+".png"
            
            selectedImage = originalImage
            imgView.image = selectedImage!
            imageSelected = "selected"
            signUpButton.isEnabled = false
            uploadImage(imgView.image)
            picker.dismiss(animated: true, completion: nil)
        }
        
    }// end Browse Profile Image Functions

    //Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citiesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpTableViewCell", for: indexPath) as! SignUpTableViewCell
        cell.cityLabel.text = citiesArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cityTxt.text = citiesArray[indexPath.row]
        cityTable.isHidden = true
    }
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }//end TableView Functions

    //Register Button
    @IBAction func SignUpBtn(_ sender: ShadowButton) {
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            if (nameTxt.text! == "" || emailTxt.text! == "" || passwordTxt.text! == "" ||
                confirm_passwordTxt.text! == "" || phoneNumTxt.text! == "" || addressTxt.text! == "" || cityTxt.text! == "")
            {
                print("Please fill all fields")
                var msg = "Please fill all fields"
                AppConfig.toastMessage(msg)
                // self.showToast(message: e)
            }
            else
            {
                SVProgressHUD.show(withStatus: "Please wait")
                
                let PasswordType: String = "manual"
                
                
                let Image: String = imageName
                
                
                
                let City: String = cityTxt.text!
                var Citykey = String()
                Citykey = dictionary.someKey(forValue: City)!
                print(Citykey)
                let City_id = Citykey
                let Name: String = nameTxt.text!
                let Email: String = emailTxt.text!
                let Password: String = passwordTxt.text!
                let Confirm_password: String = confirm_passwordTxt.text!
                let Phone_number: String = phoneNumTxt.text!
                let Address: String = addressTxt.text!
                
                let Location = ""
                
                if passwordTxt.text! == confirm_passwordTxt.text!
                {
                    SVProgressHUD.show(withStatus: "Please Wait")
                    
                    if imageSelected == "selected"
                    {
                        // uploadImage(imgView.image)
                    }
                    
                    register(name: Name, email: Email, image: Image, phone: Phone_number, address: Address, location: Location, city_id: City_id, password: Password, password_type: PasswordType)
                }
                else
                {
                    SVProgressHUD.dismiss()
                    print("Password not match")
                    let msg = "Password not match"
                    self.toastMessage(msg)
                }
            }
        }
        else{
            let msg = "No Internet Connection"
            AppConfig.toastMessage(msg)
        }
        
        
    }

    //Register
    func register(name: String, email: String, image: String, phone: String, address: String, location: String, city_id: String, password: String, password_type: String) {
        let url = AppConfig.signupUrl
        
        let parameters = ["name": name,"email": email,"image" : image,"phone" : phone,"address" : address,"location" : location,"city_id" : city_id,"password" : password,"password_type" : password_type]
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success(let json):
                print(response)
                
                if let error = (json as! [String: Any])["error"] as? Bool {
                    
                    if error == false {
                        
                        if let data = (json as! [String: Any])["user"] as? NSDictionary {
                            
                            self.Tray.Active = data["active"] as! String
                            self.Tray.Address = data["address"] as! String
                            self.Tray.Area_Id = data["area_id"] as! String
                            self.Tray.City_Id = data["city_id"] as! String
                            self.Tray.Created_At = data["created_at"] as! String
                            self.Tray.District_Id = data["district_id"] as! String
                            self.Tray.Email = data["email"] as! String
                            self.Tray.Id = data["id"] as! String
                            self.Tray.Image = data["image"] as! String
                            self.Tray.Modified = data["modified"] as! String
                            self.Tray.Name = data["name"] as! String
                            self.Tray.Phone = data["phone"] as! String
                            self.Tray.Role = data["role"] as! String
                            self.Tray.Token = data["token"] as! String
                            self.Tray.Uc_Id = data["uc_id"] as! String
                            self.Tray.City_Name = data["city_name"] as! String
                          //  self.Tray.Area_Name = data["area_name"] as! String
                            
                            user_role = self.Tray.Role
                            
                            if let tokens = data["tokens"] as? String {
                                
                                self.Tray.CUSTOMER_TOKENS = tokens
                            }
                            
                            self.autoLogin(address: self.Tray.Address, area_id: self.Tray.Area_Id, city_id: self.Tray.City_Id, district_id: self.Tray.District_Id, email: self.Tray.Email, image: self.Tray.Image, name: self.Tray.Name, phone: self.Tray.Phone, uc_id: self.Tray.Uc_Id, role: self.Tray.Role, city_name: self.Tray.City_Name)//, area_name: self.Tray.Area_Name)
                            
                        }
                        
                        // completion(true)
                        
                    } else {
                        
                        let error_msg = (json as! [String: Any])["error_msg"] as? String
                        
                        self.Tray.FAILD_MESSAGE = error_msg!
                        
                        
                        
                        let error_msgES = (json as! [String: Any])["error_msgES"] as? String
                        SVProgressHUD.dismiss()
                        let msg = "Email & password don't matched"
                        self.toastMessage(msg)
                        
                        
                    }
                    
                }
                
                break
               
            case .failure(let error):
                print("error")
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func autoLogin(address: String, area_id: String, city_id: String, district_id: String, email: String, image: String, name: String, phone: String, uc_id: String, role: String, city_name: String) -> Void {
        //, area_name: String
        
        UserDefaults.standard.set(address, forKey:"Address")
        UserDefaults.standard.set(area_id, forKey:"Area_Id")
        UserDefaults.standard.set(city_id, forKey:"City_Id")
        UserDefaults.standard.set(district_id, forKey:"District_Id")
        UserDefaults.standard.set(email, forKey:"Email")
        UserDefaults.standard.set(image, forKey:"Image")
        UserDefaults.standard.set(name, forKey:"Name")
        UserDefaults.standard.set(phone, forKey:"Phone")
        UserDefaults.standard.set(uc_id, forKey:"Uc_Id")
        UserDefaults.standard.set(role, forKey:"role")
        UserDefaults.standard.set(city_name, forKey:"City_Name")
      //  UserDefaults.standard.set(area_name, forKey:"Area_Name")
        
        
        
        SVProgressHUD.dismiss()
        let a = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigation")
        self.present(a!, animated: true, completion: nil)
        UserDefaults.standard.synchronize()
        
        print("Calling")
    }
    
    //Image Upload
    func uploadImage(_ profileImage:UIImage?){
        
        SVProgressHUD.show(withStatus: "Uploading Image")
        
        
        let url = AppConfig.imageUploadUrl //"http://enscyd.com/cleanpakistan/imageupload.php"
        
        guard let profileImage = profileImage else {
            return
        }

        guard let imageDATA = profileImage.jpegData(compressionQuality: 0.45) else { return }
   //     let email = emailTxt.text!
        let imageData = imageDATA //profileImage.pngData()!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "image",fileName: "\(self.imageName)", mimeType: "image/png")
        },to:url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    
                    let with100  =  progress.fractionCompleted * 100
                    
                    let xx = Int(with100)
                    
                    print("Upload Progress With Int: \(xx)")
                    
                })
                
                upload.responseJSON { response in
                  //  print(response.result.value)

                    SVProgressHUD.dismiss()
                    print("Done With Uploading")
                    self.picUploadStatus = "true"
                    self.signUpButton.isEnabled = true
                 //   self.imageUploaded = true
                }
                
            case .failure(let encodingError):
                
                //  self.profileImage.image
                print(encodingError)
            }
        }
    }
    // get cities from Api
    func getCityData(){
        let url = AppConfig.getAllCitiesUrl //"http://www.enscyd.com/cleanpakistan/showAllCity.php"
        print(url)
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON
            {responce in
                switch responce.result{
                case .success:
                    if let json = responce.result.value{
                        let JSON = json as! NSDictionary
                        print(JSON.allKeys)
                        print(JSON)
                        for res in JSON{
                            if ("\(res.key)" == "records")
                            {
                                record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                for innerResult in record{
                                   // print(innerResult)

                                    let dicFrmArray = innerResult
                                    
                                        if ((dicFrmArray["city_name"] as? NSNull) == nil && dicFrmArray["id"] != nil ){
                                            let City = (dicFrmArray["city_name"])
                                            let Id = (dicFrmArray["id"] )
                                            self.citiesArray.append(City as! String)
                                            self.citiesIdArray.append(Id as! String)
                                    }
   
                                }
                                
                                for (index, element) in self.citiesIdArray.enumerated() {
                                    self.dictionary[element] = self.citiesArray[index]
                                }
                                print(self.dictionary)
                                self.cityTable.reloadData()
                            }
                        }
                        
                    }
                    else{
                        print("Failure")
                    }
                    break
                case .failure:
                    if let json = responce.result.value{
                        print(json)
                    }
                    break
                }
        }
        
    }
    
    
    // func Image to base64 Conversion
    


}
// Dictionary
extension Dictionary where Value: Equatable {
    func someKey(forValue val: Value) -> Key? {
        return first(where: { $1 == val })?.key
    }
}

