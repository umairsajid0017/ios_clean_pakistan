//
//  ComplainViewController.swift
//  CleanPakistan
//
//  Created by Yousaf Shafiq on 12/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

var garbageTypeId: String = ""
class ComplainViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    
    @IBOutlet weak var collectionView:UICollectionView!
    
    
    var totalComplains = [Complain]()
    var garbage_data = [Garbage]()
    var garbage_images = [UIImage]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addNavBars("Complains")
    
        getGarbageData()
        self.prepareModel()

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: (self.view.frame.width/2) - 15 , height: (self.view.frame.width/2)  )
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 10
        collectionView!.collectionViewLayout = layout
       // collectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //return totalComplains.count
        return garbage_data.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComplainCollectionViewCell", for: indexPath) as! ComplainCollectionViewCell
        
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(selectButtonSelected), for: .touchUpInside)
        let complain = totalComplains[indexPath.item]
        
        let imageName = garbage_data[indexPath.row].garbageImage
        let remoteImageURL = URL(string: AppConfig.downloadImageUrl+imageName)!
        
        // Use Alamofire to download the image
        Alamofire.request(remoteImageURL).responseData { response in
            guard let image = UIImage(data:response.data!) else {
                // Handle error
                print("No Image")
                return
            }
            let imageData = image.pngData()
            
            cell.Picture.image = UIImage(data : imageData!)
            print("Image set")
        }
       // cell.Picture.image = UIImage(named: complain.complainImage)
        
        cell.backView.backgroundColor = UIColor.colorWithHex(hexString: complain.complainColor)
        
       // cell.PictureLabel.text = complain.complainTitle
        cell.PictureLabel.text = garbage_data[indexPath.row].garbageName

        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true

        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 0.5
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        
        cell.backgroundColor = UIColor.clear
        
        
        return cell
    }
    @objc func selectButtonSelected(sender: UIButton){
        
        print(sender.tag)
        let tag:NSInteger = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        
        let cell = collectionView.cellForItem(at: indexPath) as! UICollectionViewCell
        //let garbageName = garbage_data[indexPath.row].id
        garbageTypeId = garbage_data[indexPath.row].id
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
    }
    /*
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        garbageTypeId = garbage_data[indexPath.row].id
        
        let a = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostViewController")
        //self.present(a!, animated: true, completion: nil)
        self.navigationController?.pushViewController(a!, animated: true)
        
     //   print (indexPath.item)
    }
    */
    func getGarbageData(){
        let url = AppConfig.garbageUrl
        print(url)
        var record = [NSDictionary]()
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON
            {responce in
                switch responce.result{
                case .success:
                    if let json = responce.result.value{
                        let JSON = json as! NSDictionary
                        print(JSON.allKeys)
                        print(JSON)
                        for res in JSON{
                            if ("\(res.key)" == "records")
                            {
                                record = JSON.object(forKey: res.key as! String) as! [NSDictionary]
                                for innerResult in record{
                                    // print(innerResult)
                                    
                                    let dicFrmArray = innerResult
                                    
                                    if ((dicFrmArray["id"] as? NSNull) == nil && dicFrmArray["garbage_name"] != nil && dicFrmArray["garabage_image"] != nil ){
                                        let Id = (dicFrmArray["id"])
                                        let Garbage_Name = (dicFrmArray["garbage_name"] )
                                        let Garbage_Image = (dicFrmArray["garabage_image"] )
                                     
                                        let data = Garbage(id: Id as! String, garbageName: Garbage_Name as! String, garbageImage: Garbage_Image as! String)
                                       
                                        self.garbage_data.append(data)
                                    }
                                    
                                }
                               
                            }
                        }
                        self.collectionView.reloadData()
                    }
                    else{
                        print("Failure")
                    }
                    break
                case .failure:
                    if let json = responce.result.value{
                        print(json)
                    }
                    break
                }
        }
        
    }
    
}




extension ComplainViewController{
    //EF5350 9CCC65 FFCA28
    func prepareModel(){
        let complainGarbage = Complain(complainImage: "garbage", complainColor: "EF5350", complainTitle: "Garbage")
        
        let complainSewerage = Complain(complainImage: "tap", complainColor: "9CCC65", complainTitle: "Sewerage Leakage")
        
        let complainTrash = Complain(complainImage: "rubbish-bin", complainColor: "FFCA28", complainTitle: "Trash not Lifted")
        
        let complainRoad = Complain(complainImage: "road", complainColor: "42A5F5", complainTitle: "Dirty Road")
        
        let complaincolor1 = Complain(complainImage: "road", complainColor: "88C5F5", complainTitle: "Dirty Road")
        
        let complaincolor2 = Complain(complainImage: "road", complainColor: "66A5F5", complainTitle: "Dirty Road")
        
        let complaincolor3 = Complain(complainImage: "road", complainColor: "7HT5F5", complainTitle: "Dirty Road")
        
        let complaincolor4 = Complain(complainImage: "road", complainColor: "1WD5F5", complainTitle: "Dirty Road")
        
        let complaincolor5 = Complain(complainImage: "road", complainColor: "8BF5F5", complainTitle: "Dirty Road")
        
        let complaincolor6 = Complain(complainImage: "road", complainColor: "76F5F5", complainTitle: "Dirty Road")
        
        let complaincolor7 = Complain(complainImage: "road", complainColor: "81D5F5", complainTitle: "Dirty Road")
        
        let complaincolor8 = Complain(complainImage: "road", complainColor: "22S5F5", complainTitle: "Dirty Road")
        
        let complaincolor9 = Complain(complainImage: "road", complainColor: "66C5F5", complainTitle: "Dirty Road")
        
        totalComplains.append(complainGarbage)
        totalComplains.append(complainSewerage)
        totalComplains.append(complainTrash)
        totalComplains.append(complainRoad)
        totalComplains.append(complaincolor1)
        totalComplains.append(complaincolor2)
        totalComplains.append(complaincolor3)
        totalComplains.append(complaincolor4)
        totalComplains.append(complaincolor5)
        totalComplains.append(complaincolor6)
        totalComplains.append(complaincolor7)
        totalComplains.append(complaincolor8)
        totalComplains.append(complaincolor9)
       // self.collectionView.reloadData()
    }
    
}
