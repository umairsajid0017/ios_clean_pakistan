//
//  LoginViewController.swift
//  CleanPakistan
//
//  Created by Apple on 25/10/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

struct User {
    var Active = String()
}
class Data {
    var Active = String()
    var Address = String()
    var Area_Id = String()
    var City_Id = String()
    var Created_At = String()
    var District_Id = String()
    var Email = String()
    var Id = String()
    var Image = String()
    var Modified = String()
    var Name = String()
    var Phone = String()
    var Role = String()
    var Token = String()
    var Uc_Id = String()
    var City_Name = String()
    var Area_Name = String()
    var CUSTOMER_TOKENS = String()
    var FAILD_MESSAGE = String()
    var FAILD_MESSAGE_ES = String()
    
    
}

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var forgetPasswordView: UIView!
    
    
    
    let Tray = Data()
    override func viewDidLoad() {
 
        super.viewDidLoad()
        forgetPasswordView.isHidden = true
        //Session but not working here
        if(UserDefaults.standard.value(forKey: "Email") as? String  != nil &&  UserDefaults.standard.value(forKey: "Name") as? String != nil){
            let a = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigation")
            self.present(a!, animated: true, completion: nil)
  
        }
        // Do any additional setup after loading the view.
    }
    
    //Session
    override func viewDidAppear(_ animated: Bool) {
        if(UserDefaults.standard.value(forKey: "Email") as? String  != nil &&  UserDefaults.standard.value(forKey: "Name") as? String != nil){
            let a = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigation")
            self.present(a!, animated: true, completion: nil)
    }
    
}
    @IBAction func loginBtn(_ sender: UIButton) {
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            if (emailTxtField.text! == "" || passwordTxtField.text! == "" )
            {
                let msg = "Please fill all fields"
                AppConfig.toastMessage(msg)
            }
            else
            {
                let emailtxt: String = emailTxtField.text!
                let passtxt: String = passwordTxtField.text!
                login(useremail: emailtxt, user_password: passtxt)
            }
        }
        else{
            let msg = "No Internet Connection"
            AppConfig.toastMessage(msg)
        }
   
    }
    
    @IBAction func forgetPasswordAction(_ sender: Any) {
     //   emailTxtField.text! = ""
     //   passwordTxtField.text! = ""
     //   forgetPasswordView.isHidden = false
        
    }
    @IBAction func sendEmailAction(_ sender: Any) {
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            if (emailTxtField.text! == "")
            {
                let msg = "Please fill all fields"
                AppConfig.toastMessage(msg)
            }
            else
            {
                let forgotemailtxt: String = emailTxtField.text!
                forgetPasword(useremail: forgotemailtxt)
                
            }
        }
        else{
            let msg = "No Internet Connection"
            AppConfig.toastMessage(msg)
        }
    }
    
    func forgetPasword(useremail: String)
    {
   
        SVProgressHUD.show(withStatus: "Please Wait")
        let url = AppConfig.forgetUrl
        
        let parameters = ["email": useremail]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success:
                print(response)
                let alert = UIAlertController(title: "Alert", message: " Check Your Email", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                self.forgetPasswordView.isHidden = true
                self.emailTxtField.text! = ""
                SVProgressHUD.dismiss()
                break
                
            case .failure:
                SVProgressHUD.dismiss()
                let msg = "Invalid Email"
                AppConfig.toastMessage(msg)
                break
                
               
            }
            
        }
        
    }
    
    
    func login(useremail: String,user_password: String) -> Void {
   

        SVProgressHUD.show(withStatus: "Please Wait")
        let url = AppConfig.loginUrl
        let parameters = ["email": useremail,"password": user_password,"password_type" : "manual"]
        
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON {
            response in
            switch (response.result) {
            case .success(let json):
                print(response)
                
                if let error = (json as! [String: Any])["error"] as? Bool {
                    
                    if error == false {
                        
                        if let data = (json as! [String: Any])["user"] as? NSDictionary {

                            self.Tray.Active = data["active"] as! String
                            self.Tray.Address = data["address"] as! String
                            self.Tray.Area_Id = data["area_id"] as! String
                            self.Tray.City_Id = data["city_id"] as! String
                            self.Tray.Created_At = data["created_at"] as! String
                            self.Tray.District_Id = data["district_id"] as! String
                            self.Tray.Email = data["email"] as! String
                            self.Tray.Id = data["id"] as! String
                            self.Tray.Image = data["image"] as! String
                            self.Tray.Modified = data["modified"] as! String
                            self.Tray.Name = data["name"] as! String
                            self.Tray.Phone = data["phone"] as! String
                            self.Tray.Role = data["role"] as! String
                            self.Tray.Token = data["token"] as! String
                            self.Tray.Uc_Id = data["uc_id"] as! String
                            self.Tray.City_Name = data["city_name"] as! String
                            self.Tray.Area_Name = data["area_name"] as! String
                            
                            if let tokens = data["tokens"] as? String {
                                
                                self.Tray.CUSTOMER_TOKENS = tokens
                            }
                            self.autoLogin(address: self.Tray.Address, area_id: self.Tray.Area_Id, city_id: self.Tray.City_Id, district_id: self.Tray.District_Id, email: self.Tray.Email, image: self.Tray.Image, name: self.Tray.Name, phone: self.Tray.Phone, uc_id: self.Tray.Uc_Id, user_role: self.Tray.Role, city_name: self.Tray.City_Name, area_name: self.Tray.Area_Name)
                            
                        }
                        
                       // completion(true)
                        
                    } else {
                        
                        let error_msg = (json as! [String: Any])["error_msg"] as? String
                       // print(error_msg)
                        self.Tray.FAILD_MESSAGE = error_msg!
                        
                        
                        
                        let error_msgES = (json as! [String: Any])["error_msgES"] as? String
                        SVProgressHUD.dismiss()
                        let msg = "Email & password don't matched"
                        AppConfig.toastMessage(msg)

                        
                    }
                    
                }
                
                break

            case .failure(let error):
                
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    
                    self.Tray.FAILD_MESSAGE = error.localizedDescription
                    
                   // completion(false)
            }
        }
            
        }
        
    }
    
    func autoLogin(address: String, area_id: String, city_id: String, district_id: String, email: String, image: String, name: String, phone: String, uc_id: String, user_role: String, city_name: String, area_name: String) -> Void {
        
        UserDefaults.standard.set(address, forKey:"Address")
        UserDefaults.standard.set(area_id, forKey:"Area_Id")
        UserDefaults.standard.set(city_id, forKey:"City_Id")
        UserDefaults.standard.set(district_id, forKey:"District_Id")
        UserDefaults.standard.set(email, forKey:"Email")
        UserDefaults.standard.set(image, forKey:"Image")
        UserDefaults.standard.set(name, forKey:"Name")
        UserDefaults.standard.set(phone, forKey:"Phone")
        UserDefaults.standard.set(uc_id, forKey:"Uc_Id")
        UserDefaults.standard.set(user_role, forKey: "role")
        UserDefaults.standard.set(city_name, forKey:"City_Name")
        UserDefaults.standard.set(area_name, forKey:"Area_Name")
        
        SVProgressHUD.dismiss()
        let a = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigation")
       // self.navigationController?.pushViewController(a!, animated: true)
          self.present(a!, animated: true, completion: nil)
        UserDefaults.standard.synchronize()
        
        print("Calling")
    }
    
}
