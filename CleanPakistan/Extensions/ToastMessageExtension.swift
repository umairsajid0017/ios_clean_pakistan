//
//  ToastMessageExtension.swift
//  CleanPakistan
//
//  Created by Apple on 03/11/2019.
//  Copyright © 2019 Yousaf Shafiq. All rights reserved.
//

import Foundation
import UIKit

// Toast Extension
extension SignUpViewController {
    func toastMessage(_ message: String){
        guard let window = UIApplication.shared.keyWindow else {return}
        let messageLbl = UILabel()
        messageLbl.text = message
        messageLbl.textAlignment = .center
        // messageLbl.font = UIFont.systemFont(ofSize: 13)
        messageLbl.font = UIFont(name: "Montserrat-Light", size: 12.0)
        messageLbl.textColor = .white
        messageLbl.backgroundColor = UIColor(white: 0, alpha: 0.8)
        let textSize:CGSize = messageLbl.intrinsicContentSize
        let labelWidth = min(textSize.width, window.frame.width - 40)
        messageLbl.frame = CGRect(x: 20, y: window.frame.height - 130, width: labelWidth + 20, height: textSize.height + 10)
        messageLbl.center.x = window.center.x
        messageLbl.layer.cornerRadius = messageLbl.frame.height/3
        messageLbl.layer.masksToBounds = true
        window.addSubview(messageLbl)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            UIView.animate(withDuration: 0.9, animations: {
                messageLbl.alpha = 0
            }) { (_) in
                messageLbl.removeFromSuperview()
            }
        }
    }
    
}


